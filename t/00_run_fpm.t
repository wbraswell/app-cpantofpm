#!/usr/bin/perl

# [[[ HEADER ]]]
use strict;
use warnings;

# disable RPerl itself
package void; 1;
package boolean; 1;
package number; 1;
package string; 1;
package string_arrayref; 1;
package integer_hashref; 1;
package string_hashref; 1;
package string_arrayref_hashref; 1;
package string_hashref_arrayref; 1;
package string_hashref_hashref; 1;
package integer; 1;
package filehandleref; 1;
package main;

# [[[ INCLUDES ]]]
use Test2::V0;
use English;
use File::Temp;
use Cwd qw(getcwd);
use MetaCPAN::Client;
use App::CPANtoFPM;

use FindBin qw($Bin);
use lib "$Bin/lib";
use TestHelpers qw(clean_test_directory);

# [[[ OPERATIONS ]]]
my string $ORIGINAL_CURRENT_DIR;
my File::Temp $TEMP_DIR;
my string $WORK_DIR;
my string $CURRENT_DIR;

subtest 'initialize run_fpm()\'s testing environment' => sub {
    $ORIGINAL_CURRENT_DIR = getcwd() or die('failed to get cwd: ', $OS_ERROR, "\n");
    $TEMP_DIR = File::Temp->newdir(DIR => '/tmp', TEMPLATE => 'cpan2fpmRUNFPM_test_tmpXXXXXX');
    $WORK_DIR = $TEMP_DIR->dirname() . '/work_dir';
    $CURRENT_DIR = $TEMP_DIR->dirname() . '/current_dir';

    mkdir($WORK_DIR)
      or die('failed to mkdir ', $WORK_DIR, ': ', $OS_ERROR, "\n");

    mkdir($CURRENT_DIR)
      or die('failed to mkdir ', $CURRENT_DIR, ': ', $OS_ERROR, "\n");

    chdir($CURRENT_DIR)
      or die('failed to chdir to ', $CURRENT_DIR, ': ', $OS_ERROR, "\n");

    pass();
};

subtest 'run_fpm()' => sub {

    ### TEST: VALIDATE run_fpm()'s BASIC BEHAVIOR ON A SUCCESSFUL INVOCATION

    my boolean $run_fpm_succeeded = ok(
        lives { App::CPANtoFPM::run_fpm('Array::Utils', 'rpm', '0.4', $WORK_DIR, []) },
        'run_rpm() terminates successfully when given valid arguments'
    ) or note($EVAL_ERROR);

    if ($run_fpm_succeeded) {
        ok(
            -f $CURRENT_DIR . '/perl-Array-Utils-0.4-1.noarch.rpm',
            'run_fpm() created a RPM package for a specific version of Array::Utils (0.4) in the processes working directory'
        );

        ok(
            -f $WORK_DIR . '/Array::Utils.stdout',
            'run_fpm() creates $input_module.stdout file in $work_dir'
        );

        subtest 'run_fpm() configures FPM to use $work_dir as its working dir' => sub {
            opendir(my filehandleref $WORK_DH, $WORK_DIR)
              or die('failed to opendir ',  $WORK_DIR, ': ', $OS_ERROR, "\n");

            my string_arrayref $work_dir_contents = [ readdir $WORK_DH ];

            closedir($WORK_DH)
              or die('failed to closedir ', $WORK_DIR, ': ', $OS_ERROR, "\n");

            my boolean $has_fpm_staging_dir = 0;

            foreach my string $file (@{$work_dir_contents}) {
                if ($file =~ /^package-cpan-staging/) {
                    $has_fpm_staging_dir = 1;
                }
            }

            ok($has_fpm_staging_dir);
        };
    }

    ### TEST: run_fpm() INSTALLS THE LATEST VERSION OF A MODULE IF IT IS GIVEN AN EMPTY STRING FOR ITS $version_template ARGUMENT

    clean_test_directory($TEMP_DIR);

    $run_fpm_succeeded = ok(
        lives { App::CPANtoFPM::run_fpm('Array::Utils', 'rpm', q{}, $WORK_DIR, []) },
        'run_rpm() terminates successfully when $version_template is passed as an empty string'
    ) or note($EVAL_ERROR);

    if ($run_fpm_succeeded) {
        # Find the latest version of Array::Utils
        my string $array_utils_latest_version = MetaCPAN::Client->new->download_url('Array::Utils')->data->{version};

        ok(
            -f $CURRENT_DIR . '/perl-Array-Utils-' . $array_utils_latest_version . '-1.noarch.rpm',
            'run_fpm() packages latest version of $inpuy_module if $version_template set to empty string'
        );
    }

    ### TEST: run_fpm() CAN CONFIGURE FPM TO ADD OS SPECIFIC DEPS TO THE OUTPUTTED PACKAGE.

    # TODO: Currently, the only way to test this is to observe the spec file that FPM outputted, which is really more of a test for FPM than cpan2fpm. If we were to extract out the creation of the FPM command string into its own subroutine (create_fpm_command_string()), then we could test that the FPM command we generate sets the proper flags for specifying OS-specific dependencies.

    clean_test_directory($TEMP_DIR);

    $run_fpm_succeeded = ok(
        lives { App::CPANtoFPM::run_fpm('Array::Utils', 'rpm', '0.4', $WORK_DIR, ['FOO_PACKAGE']) },
        'run_fpm() terminates successfully when passed OS specific deps in $OS_SPECIFIC_DEPENDENCIES_DISTRIBUTION argument'
    );

    if ($run_fpm_succeeded) {
        subtest 'run_rpm() configured FPM to output an RPM spec file in the working directory' => sub {
            opendir(my filehandleref $WORK_DH, $WORK_DIR)
              or die('failed to opendir ', $WORK_DIR, ': ', $OS_ERROR, "\n");

            my string_arrayref $work_dir_dirs = [ readdir $WORK_DH ];

            closedir($WORK_DH)
              or die('failed to closedir ', $WORK_DIR, ': ', $OS_ERROR, "\n");

            my string $rpm_build_dir_basename = '';
            foreach my string $file (@{$work_dir_dirs}) {
                if ($file =~ /^package-rpm-build/) {
                    $rpm_build_dir_basename = $file;
                }
            }

            my string $spec_file = $WORK_DIR . '/' . $rpm_build_dir_basename . '/SPECS/perl-Array-Utils.spec';

            open(my filehandleref $SPEC_FH, '<', $spec_file)
              or die('failed to open file ', $spec_file, ': ', $OS_ERROR, "\n");

            my boolean $spec_file_requires_foo_package = 0;

            while (my string $line = <$SPEC_FH>) {
                chomp $line;
                if ($line eq 'Requires: FOO_PACKAGE') {
                    $spec_file_requires_foo_package = 1;
                }
            }

            close $SPEC_FH
              or die('failed to close file ', $spec_file, ': ', $OS_ERROR, "\n");

            ok($spec_file_requires_foo_package);
        };
    }

    ### TEST: run_fpm() KILLS THE PROGRAM IF ANY OF ITS ARGUMENTS ARE INVALID

    clean_test_directory($TEMP_DIR);

    ok(
        dies { App::CPANtoFPM::run_fpm('INVALID_INPUT_MODULE', 'rpm', '0.4', $WORK_DIR, []) },
        'run_fpm() dies when given invalid $input_module'
    );

    clean_test_directory($TEMP_DIR);

    ok(
        dies { App::CPANtoFPM::run_fpm('Array::Utils', 'INVALID_OUTPUT_FORMAT', '0.4', $WORK_DIR, []) },
        'run_fpm() dies when given invalid $output_format'
    );

    clean_test_directory($TEMP_DIR);

    ok(
        dies { App::CPANtoFPM::run_fpm('Array::Utils', 'rpm', 'INVALID_VERSION', $WORK_DIR, []) },
        'run_fpm() dies when given invalid $version_template'
    );

    clean_test_directory($TEMP_DIR);

    ok(
        dies { App::CPANtoFPM::run_fpm('Array::Utils', 'rpm', '0.4', 'INVALID_WORK_DIR', []) },
        'run_fpm() dies when given non-existent $work_dir'
    );

    clean_test_directory($TEMP_DIR);

    ok(
        dies { App::CPANtoFPM::run_fpm('Array::Utils', 'rpm', '0.4', $WORK_DIR, 'INVALID_OS_SPECIFIC_DEPENDENCIES_DISTRIBUTION') },
        'run_fpm() dies when given invalid $OS_SPECIFIC_DEPENDENCIES_DISTRIBUTION'
    );

    # $TEMP_DIR cannot be deleted if its path is part of the cwd.
    chdir($ORIGINAL_CURRENT_DIR)
      or die('failed to chdir to ', $ORIGINAL_CURRENT_DIR, ': ', $OS_ERROR, "\n");
};

done_testing();
