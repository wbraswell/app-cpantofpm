#!/usr/bin/perl

# [[[ HEADER ]]]
use strict;
use warnings;

# disable RPerl itself
package void; 1;
package boolean; 1;
package number; 1;
package string; 1;
package string_arrayref; 1;
package integer_hashref; 1;
package string_hashref; 1;
package string_arrayref_hashref; 1;
package string_hashref_arrayref; 1;
package string_hashref_hashref; 1;
package integer; 1;
package filehandleref; 1;
package main;

# [[[ INCLUDES ]]]
use Test2::V0;
use English;
use App::CPANtoFPM;

# [[[ OPERATIONS ]]]

subtest 'update_arch()' => sub {
    is(
        App::CPANtoFPM::update_arch(q{}, 'foo'),
        'foo',
        'returns $arch_new if $arch_master is empty string'
    );

    is(
        App::CPANtoFPM::update_arch('noarch', 'foo'),
        'foo',
        q{returns $arch_new if $arch_master is 'noarch'}
    );

    is(
        App::CPANtoFPM::update_arch('foo', 'noarch'),
        'foo',
        q{returns $arch_master if $arch_new is 'noarch'}
    );

    is(
        App::CPANtoFPM::update_arch('foo', 'foo'),
        'foo',
        'if $arch_master eq $arch_new, returns their value'
    );

    like(
        dies { App::CPANtoFPM::update_arch('foo', 'bar') },
        qr/Package architecture mis-match, found both 'foo' and 'bar'/,
        q{dies if $arch_master ne $arch_new, and neither of them eq 'noarch'}
    );
};

done_testing();
