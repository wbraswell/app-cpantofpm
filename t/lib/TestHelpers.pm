# [[[ HEADER ]]]
package TestHelpers;
use strict;
use warnings;

# disable RPerl itself
package void; 1;
package boolean; 1;
package number; 1;
package string; 1;
package string_arrayref; 1;
package integer_hashref; 1;
package string_hashref; 1;
package string_arrayref_hashref; 1;
package string_hashref_arrayref; 1;
package string_hashref_hashref; 1;
package integer; 1;
package filehandleref; 1;
package TestHelpers;

# [[[ EXPORTS ]]]
use Exporter qw(import);
our @EXPORT_OK = qw(clean_test_directory);

# [[[ INCLUDES ]]]
use Test2::V0;
use English;
use Cwd qw(getcwd);
use File::Path qw(remove_tree);

# [[[ OPERATIONS ]]]

# clean_test_directory() can be used to clean a testing directory to ensure that tests are run in a clean environment. It takes a single argument that should be the root of the testing temp directory. All directories that are at the toplevel of this temporary directory will be cleared such that they are empty. All files at the toplevel of this directory will be deleted.
sub clean_test_directory {
    (my string $test_dir_root) = @ARG;

print {*STDERR} 'in clean_test_directory(), received $test_dir_root = ', $test_dir_root, "\n";

    # We wrap the code that may die in a subtest, so if it does die we only get a test failure instead of crashing the test script.
    subtest 'clean test directory ' . $test_dir_root => sub {
        my string $original_cwd = getcwd()
          or die('failed to get cwd: ', $OS_ERROR, "\n");

        chdir($test_dir_root)
          or die('failed to chdir to ', $test_dir_root, ': ', $OS_ERROR, "\n");

        opendir(my filehandleref $TEST_DIR_ROOT_DH, $test_dir_root)
          or die('failed to opendir ', $test_dir_root, ': ', $OS_ERROR, "\n");

        foreach my string $file_basename (readdir $TEST_DIR_ROOT_DH) {
            if (($file_basename eq '.') or ($file_basename eq '..')) {
                next;
            }

            my string $file_full_path = $test_dir_root . '/' . $file_basename;

            if (-d $file_full_path) {
                remove_tree($file_full_path)
                  or die('failed to recursively delete directory ', $file_full_path, ': ', $OS_ERROR, "\n");

                mkdir($file_full_path)
                  or die('failed to mkdir ', $file_full_path, ': ', $OS_ERROR, "\n");
            }
            else {
                unlink($file_full_path)
                  or die('failed to unlink file ', $file_full_path, ': ', $OS_ERROR, "\n");
            }
        }

        closedir($TEST_DIR_ROOT_DH)
          or die('failed to closedir ', $test_dir_root, ': ', $OS_ERROR, "\n");

        chdir($original_cwd)
          or die('failed to chdir to ', $original_cwd, ': ', $OS_ERROR, "\n");

print {*STDERR} 'in clean_test_directory(), successfully cleaned directory ', $test_dir_root, "\n";

        pass();
    };
}

1;
