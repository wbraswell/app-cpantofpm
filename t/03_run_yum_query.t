#!/usr/bin/perl

# [[[ HEADER ]]]
use strict;
use warnings;

# disable RPerl itself
package void; 1;
package boolean; 1;
package number; 1;
package string; 1;
package string_arrayref; 1;
package integer_hashref; 1;
package string_hashref; 1;
package string_arrayref_hashref; 1;
package string_hashref_arrayref; 1;
package string_hashref_hashref; 1;
package integer; 1;
package filehandleref; 1;
package main;

# [[[ INCLUDES ]]]
use Test2::V0;
use English;
use File::Temp;
use App::CPANtoFPM;

# [[[ OPERATIONS ]]]
my File::Temp $TEMP_DIR;
my string $YUM_TESTING_CONFIG;

subtest 'initialize run_yum_query()\'s testing environment' => sub {
    $TEMP_DIR = File::Temp->newdir(DIR => '/tmp', TEMPLATE => 'cpan2fpm_test_tmpXXXXXX');
    $YUM_TESTING_CONFIG = $TEMP_DIR->dirname() . '/yum_test_config';
    pass();
};

subtest 'run_yum_query()' => sub {
    open(my $YUM_CONFIG_FH, '>', $YUM_TESTING_CONFIG)
      or die('failed to open file ', $YUM_TESTING_CONFIG, ': ', $OS_ERROR, "\n");

    print $YUM_CONFIG_FH '[fedora36]', "\n", 'metalink=https://mirrors.fedoraproject.org/metalink?repo=fedora-36&arch=aarch64', "\n";
    print $YUM_CONFIG_FH '[fedora37]', "\n", 'metalink=https://mirrors.fedoraproject.org/metalink?repo=fedora-37&arch=aarch64', "\n";

    close($YUM_CONFIG_FH)
      or die('failed to close file ', $YUM_TESTING_CONFIG, ': ', $OS_ERROR, "\n");

    is(
       App::CPANtoFPM::run_yum_query('perl(IO::Compress::Gzip)', $YUM_TESTING_CONFIG),
       ['fedora36', 'fedora37'],
       'returns arrayref of repos containing module'
    );

    is(
       App::CPANtoFPM::run_yum_query('perl(not@a@module)', $YUM_TESTING_CONFIG),
       [],
       'returns empty arrayref when passes a non-existent module'
    );
};

done_testing();
