# COPYRIGHT

# [[[ HEADER ]]]
package App::CPANtoFPM;
use strict;
use warnings;
our $VERSION = 0.001_000;

# disable RPerl itself
package void; 1;
package boolean; 1;
package number; 1;
package string; 1;
package string_arrayref; 1;
package integer_hashref; 1;
package string_hashref; 1;
package string_arrayref_hashref; 1;
package string_hashref_arrayref; 1;
package string_hashref_hashref; 1;
package integer; 1;
package filehandleref; 1;
package App::CPANtoFPM;

# [[[ CRITICS ]]]
## no critic qw(ProhibitUselessNoCritic ProhibitMagicNumbers RequireCheckedSyscalls)  # USER DEFAULT 1: allow numeric values & print operator
## no critic qw(RequireInterpolationOfMetachars)  # USER DEFAULT 2: allow single-quoted control characters & sigils
## no critic qw(RequireBriefOpen)  # USER DEFAULT 5: allow open() in perltidy-expanded code

# [[[ INCLUDES ]]]
use English;
use IPC::Run3 qw(run3);
use Module::CoreList;
use CPAN;
use MetaCPAN::Client;
use version 0.77;  # for comparing version strings
use File::Copy;     # single-life module only in Perl core; for copy & move
use File::Spec;
use File::Path qw( make_path );

use Data::Dumper;
$Data::Dumper::Sortkeys = 1;

# [[[ HACK ]]]

# These are the same hardcoded global variables that are defined in
# bin/cpantofpm. They need to be defined in this module as well as bin/cpantofpm
# for purposes of modularization and testing. After the initial test suite is
# finished, one of our first priorities must be to remove these hardcoded values
# from both here and bin/cpantofpm.
my string $maintainer = q{'William N. Braswell, Jr. <william.braswell@NOSPAM.autoparallel.com>'};
my string $work_dir = $ENV{'HOME'} . '/cpantofpm_tmp';

# [[[ CONSTANTS ]]]

# DEV NOTE: keyed by distribution, not by module!
# NEED UPGRADE: include version numbers, more OS-specific info such as exact distro, move all possible into out to actual distros' metadata

our string_arrayref_hashref $OS_SPECIFIC_DEPENDENCIES = {
    # RPERL CORE FEATURES

    # NEED FIX BUG: redhat's perl-Inline package does not include C build env as runtime requirement, copy to Inline::CPP below until fixed    https://bugzilla.redhat.com/show_bug.cgi?id=1624051
    'Inline::C' => {
        deb => [qw(gcc make libc6-dev)],
        rpm => [qw(gcc make glibc-devel)],
    },
    'Inline::CPP' => {
#        deb => [qw(g++)],
#        rpm => [qw(gcc-c++)],
        deb => [qw(g++        gcc make libc6-dev)],
        rpm => [qw(gcc-c++    gcc make glibc-devel)],
    },
    'Alien::astyle' => {
        deb => [qw(astyle)],
        rpm => [qw(astyle)],
    },
    'Alien::PCRE2' => {
        deb => [qw(libpcre2-8-0 libpcre2-16-0 libpcre2-32-0 libpcre2-dev)],  # DEB, START HERE: do these work, or do we need to build new packages for PCRE2 v10.31?
        rpm => [qw(libpcre2 libpcre2-dev)],
    },
    'Alien::JPCRE2' => {
        deb => [qw(libjpcre2-dev)],  # DEB, START HERE: need build packages
        rpm => [qw(libjpcre2-dev)],
    },
    'Alien::Pluto' => {
        deb => [qw(texinfo flex bison pluto-polycc pluto-polycc-dev)],  # DEB, START HERE: need build package(s)
        rpm => [qw(texinfo flex bison pluto-polycc pluto-polycc-dev)],
    },
    'Net::SSLeay' => {  # DEV NOTE: err.h Required By RPerl Subdependency Net::SSLeay From IO::Socket::SSL From ... From Alien::*
        deb => [qw(libssl-dev)],
        rpm => [qw(openssl-devel)],
    },

    # RPERL SOON-TO-BE EXTRA FEATURES, RPerl::Support::*
    'Alien::GMP' => {
        deb => [qw(libgmp-dev)],
        rpm => [qw(gmp gmp-static gmp-devel)],
    },
    'Math::BigInt::GMP' => {  # NEED UPGRADE: when/if Math::BigInt::GMP adds Alien::GMP as CPAN dependency, remove this entire entry & rebuild packages
        deb => [qw(perl-alien-gmp)],
        rpm => [qw{perl(Alien::GMP)}],
    },
    'Alien::GSL' => {
        deb => [qw(libgsl0-dev)],
        rpm => [qw(gsl gsl-devel)],
    },
    'Math::GSL' => {  # NEED UPGRADE: when/if Math::GSL adds Alien::GSL as CPAN dependency, remove this entire entry & rebuild packages
        deb => [qw(perl-alien-gsl)],
        rpm => [qw{perl(Alien::GSL)}],
    },
    'Alien::SDL' => {  # DEV NOTE: zlib.h Required By Alien::SDL, Required By SDL, Required For Graphics In RPerl Applications
#    'Alien::zlib' => {  # NEED UPGRADE: when/if Alien::zlib is created, and SDL adds Alien::zlib as CPAN dependency, split zlib* out of Alien::SDL
        deb => [qw(zlib1g zlib1g-dev
            libsdl1.2debian libsdl1.2-dev libsdl-gfx1.2   libsdl-gfx1.2-dev libsdl-image1.2   libsdl-image1.2-dev libsdl-mixer1.2   libsdl-mixer1.2-dev libsdl-pango1 libsdl-pango-dev libsdl-sound1.2 libsdl-sound1.2-dev libsdl-ttf2.0   libsdl-ttf2.0-dev
            libsdl2-2.0     libsdl2-dev   libsdl2-gfx-1.0 libsdl2-gfx-dev   libsdl2-image-2.0 libsdl2-image-dev   libsdl2-mixer-2.0 libsdl2-mixer-dev                                                                      libsdl2-ttf-2.0 libsdl2-ttf-dev
            libjpeg-turbo8 libjpeg-turbo8-dev libtiff5 libtiff5-dev libogg0 libogg-dev libvorbis0a libvorbis-dev libfreetype6 libfreetype6-dev)],
        rpm => [qw(zlib zlib-static zlib-devel
            SDL SDL-devel SDL-static SDL_gfx SDL_gfx-devel SDL_Pango SDL_Pango-devel
            SDL2 SDL2-devel SDL2-static SDL2_gfx SDL2_gfx-devel SDL2_image SDL2_image-devel SDL2_mixer SDL2_mixer-devel SDL2_ttf SDL2_ttf-devel
            libjpeg-turbo libjpeg-turbo-devel libjpeg-turbo-static libtiff libtiff-devel libogg libogg-devel libvorbis libvorbis-devel)],
    },
    'MongoDB' => {
#    'Alien::MongoDB' => {  # NEED UPGRADE: when/if Alien::MongoDB is created, and MongoDB adds Alien::MongoDB as CPAN dependency, replace above line w/ this line & rebuild packages
        deb => [qw(pkg-config libbson-1.0-0 libbson-dev libmongoc-1.0-0 libmongoc-dev libmongocxx-3.0-0 libmongocxx-dev)],  # DEB, START HERE: rebuild C driver packages to avoid bionic release dependency, build C++ packages
        rpm => [qw(pkgconfig libbson libbson-devel mongo-c-driver-libs mongo-c-driver mongo-c-driver-devel mongo-cxx-driver-libs mongo-cxx-driver mongo-cxx-driver-devel)],
    },

    # RPERL ITSELF
    'RPerl' => {
        deb => [qw(perl libperl-dev)],
        rpm => [qw(perl-core perl-libs perl-devel)],
    },
};

# [[[ SUBROUTINES ]]]

# RECURSIVELY BUILD PACKAGES
# RECURSIVELY BUILD PACKAGES
# RECURSIVELY BUILD PACKAGES
sub recurse {
    { my string_hashref_hashref $RETURN_TYPE };  # output return value copied from last nested subroutine, determine_dependencies()
    ( my string $input_module, my string $output_format, my string $version_template, my string $work_dir, my string_hashref_hashref $dependencies_flat, my string $arch_master, my string_arrayref_hashref $OS_SPECIFIC_DEPENDENCIES ) = @ARG;  # input parameters copied from first nested subroutine, run_fpm()

print {*STDERR} 'in recurse(), received $input_module = ', q{'}, $input_module, q{'}, "\n";
print {*STDERR} 'in recurse(), received $version_template = ', q{'}, $version_template, q{'}, "\n";
###print {*STDERR} 'in recurse(), received $output_format = ', $output_format, "\n";
###print {*STDERR} 'in recurse(), received $work_dir = ', $work_dir, "\n";
print {*STDERR} 'in recurse(), received $dependencies_flat = ', Dumper($dependencies_flat);
(print {*STDERR} 'in recurse(), received $dependencies_flat->{_module_master} = ', q{'}, $dependencies_flat->{_module_master}, q{'}, "\n") if (defined $dependencies_flat->{_module_master});
print {*STDERR} 'in recurse(), received $dependencies_flat->{_stack_master} = ', Dumper($dependencies_flat->{_stack_master});
print {*STDERR} 'in recurse(), received $dependencies_flat->{_dependencies_flat_master_module} = ', Dumper($dependencies_flat->{_dependencies_flat_master_module});
print {*STDERR} 'in recurse(), received $arch_master = ', q{'}, $arch_master, q{'}, "\n";
#print {*STDERR} 'in recurse(), received $OS_SPECIFIC_DEPENDENCIES = ', Dumper($OS_SPECIFIC_DEPENDENCIES);

# SKIP CONDITIONS:

# 0a. CURRENT MODULE ALREADY IN PERL CORE (via MetaCPAN::Client) AS SINGLE-LIFE
#    if (CURRENT DISTRIBUTION IS 'perl') { update_dependencies_flat(); skip; }

# 0b. CURRENT MODULE ALREADY IN PERL CORE (via Module::CoreList) AS SINGLE-LIFE
#    if (CURRENT DISTRIBUTION IS 'perl') { update_dependencies_flat(); skip; }

# 1a. CURRENT MODULE        MATCHES          ON-STACK MODULE  (slow CPAN lookup, slow check via foreach)
#    same notes as #1b below
#    if (CURRENT MODULE MATCHES ON-STACK MODULE) {
#        if (CURRENT DISTRIBUTION MATCHES ON-STACK DISTRIBUTION) { update _stacks_partial; skip; }
#        else { die; }
#    }

# 1b. CURRENT MODULE        MATCHES ALREADY-PROCESSED MODULE  (slow CPAN lookup, fast check via exists)
#    File::Spec/PathTools  matches File::Spec/*
#    distribution * must match distribution PathTools due to CPAN namespace protection, 2 dists cannot contain 1 module w/ the exact same name File::Spec
#    if (CURRENT MODULE MATCHES ALREADY-PROCESSED MODULE) {
#        if (CURRENT DISTRIBUTION MATCHES ALREADY-PROCESSED DISTRIBUTION) { update_dependencies_flat(); skip; }
#        else { die; }
#    }

# 2a. CURRENT DISTRIBUTION  MATCHES          ON-STACK MODULE  (slow CPAN lookup, slow check via foreach)
#    same notes as #2b below
#    if (CURRENT DISTRIBUTION MATCHES ON-STACK MODULE) {
#        if (CURRENT DISTRIBUTION MATCHES ON-STACK DISTRIBUTION) { update _stacks_partial; skip; }
#        else { die; }
#    }

# 2b. CURRENT DISTRIBUTION  MATCHES ALREADY-PROCESSED MODULE  (slow CPAN lookup, fast check via exists)
#    Foo/Bar               matches Bar/*
#    distribution * must match distribution Bar due to CPAN module requirement, distribution Bar must contain module Bar (or owners of grandfathered dist Bar also own not-yet-created module Bar)
#    if (CURRENT DISTRIBUTION  MATCHES ALREADY-PROCESSED MODULE) {
#        if (CURRENT DISTRIBUTION MATCHES ALREADY-PROCESSED DISTRIBUTION) { update_dependencies_flat(); skip; }
#        else { die; }
#    }

# 3a. CURRENT DISTRIBUTION  MATCHES          ON-STACK DISTRIBUTION  (slow CPAN lookup, slow check via foreach; could short-circuit both #1a & #2a above, but slow CPAN lookup should be delayed for performance)
#    same notes as #3b below
#    if (CURRENT DISTRIBUTION MATCHES ON-STACK DISTRIBUTION) { update _stacks_partial; skip; }

# 3b. CURRENT DISTRIBUTION  MATCHES ALREADY-PROCESSED DISTRIBUTION  (slow CPAN lookup, slow check via foreach; could short-circuit both #1b & #2b above, but slow check should be delayed for performance)
#    File::Spec/PathTools  matches */PathTools
#    distribution PathTools must contain module File::Spec due to CPAN distribution name uniqueness, only one distribution can exist for each distribution name
#    if (CURRENT DISTRIBUTION  MATCHES ALREADY-PROCESSED DISTRIBUTION) { update_dependencies_flat(); skip; }

# 4a. CURRENT MODULE        MATCHES          ON-STACK DISTRIBUTION  (slow CPAN lookup, slow check via foreach; short-circuited by #3a above, DO NOT ACTUALLY IMPLEMENT)
#    same notes as #4b below
#    if (CURRENT MODULE MATCHES ON-STACK DISTRIBUTION) {
#        if (CURRENT DISTRIBUTION MATCHES ON-STACK DISTRIBUTION) { update _stacks_partial; skip; }
#        else { die; }
#    }

# 4b. CURRENT MODULE        MATCHES ALREADY-PROCESSED DISTRIBUTION  (slow CPAN lookup, slow check via foreach; short-circuited by #3b above, DO NOT ACTUALLY IMPLEMENT)
#    Foo/*                 matches */Foo
#    distribution * must match distribution Foo due to CPAN module requirement, distribution Foo must contain module Foo (or owners of grandfathered dist Bar also own not-yet-created module Bar)
#    if (CURRENT MODULE MATCHES ALREADY-PROCESSED DISTRIBUTION) {
#        if (CURRENT DISTRIBUTION MATCHES ALREADY-PROCESSED DISTRIBUTION) { update_dependencies_flat(); skip; }
#        else { die; }
#    }

    # DEV NOTE: process dependencies by module, but store distribution info as well
    my string $distribution = q{};
    my string_arrayref $OS_SPECIFIC_DEPENDENCIES_DISTRIBUTION = [];

    # MetaCPAN::Client methodology
    my MetaCPAN::Client $metacpan_client = MetaCPAN::Client->new( version => 'v1' );
    my MetaCPAN::Client::Module $module = $metacpan_client->module($input_module, { fields => 'distribution,download_url' });
    if ((exists $module->{data}->{distribution}) and (defined $module->{data}->{distribution})) {
        $distribution = $module->{data}->{distribution};
        my string $distribution_cpan_file = $module->{data}->{download_url};

=DISABLED_CPAN_SHELL
    # CPAN::Shell methodology
    # HARD-CODED EXAMPLE:
    # X/XS/XSAWYERX/perl-5.28.0.tar.gz
#    CPAN::Shell->m($input_module);  # useless, prints results, returns undef
#    my $distribution_cpan_file = CPAN::Shell->expand('Module', $input_module)->cpan_file();  # error on module 'Config', can't call cpan_file() on undefined reference
    my $module_object = CPAN::Shell->expand('Module', $input_module);
    if (defined $module_object) {
        my string $distribution_cpan_file = $module_object->cpan_file();

        # determine CPAN distribution name from module name (may be the same)
        # HARD-CODED EXAMPLE:
        # E/ET/ETHER/File-Temp-0.2306.tar.gz
        # M/MO/MONGODB/MongoDB-v2.0.0.tar.gz
        if ($distribution_cpan_file =~ m/^[A-Z]\/[A-Z][A-Z]\/[A-Z]+\/([-+\w]+)-[v.\d]+\.tar\.gz$/) {
            $distribution = $1;            
        }
        # HARD-CODED EXAMPLE:
        # M/MU/MUIR/modules/Text-Tabs+Wrap-2013.0523.tar.gz
        # F/FO/FOOBAR/modules/FooBar-v1.2.3.tar.gz
        elsif ($distribution_cpan_file =~ m/^[A-Z]\/[A-Z][A-Z]\/[A-Z]+\/modules\/([-+\w]+)-[v.\d]+\.tar\.gz$/) {
            $distribution = $1;            
        }
        # HARD-CODED EXAMPLE:
        # R/RS/RSAVAGE/Config-Tiny-2.23.tgz
        # F/FO/FOOBAR/FooBar-v1.2.3.tgz
        elsif ($distribution_cpan_file =~ m/^[A-Z]\/[A-Z][A-Z]\/[A-Z]+\/([-+\w]+)-[v.\d]+\.tgz$/) {
            $distribution = $1;            
        }
        # HARD-CODED EXAMPLE:
        # F/FO/FOOBAR/modules/FooBar-1.2.3.tgz
        # F/FO/FOOBAR/modules/FooBar-v1.2.3.tgz
        elsif ($distribution_cpan_file =~ m/^[A-Z]\/[A-Z][A-Z]\/[A-Z]+\/modules\/([-+\w]+)-[v.\d]+\.tgz$/) {
            $distribution = $1;            
        }
        else {
            die 'CPANtoFPM ERROR, CPAN Distribution Format Not Recognized: ', $distribution_cpan_file, "\n", 'dying';
        }

        # DEV NOTE, INCORRECT CODE: some distribution names contain hyphen character and should not be replaced, such as 'Text-Tabs+Wrap',
        # must replace entire CPAN::Shell methodology with MetaCPAN::Client methodology to get proper distribution name
        $distribution =~ s/-/::/gxms;  # replace hyphens with double-colons
=cut

print {*STDERR} 'in recurse(), have $distribution_cpan_file = ', q{'}, $distribution_cpan_file, q{'}, "\n";
print {*STDERR} 'in recurse(), have $distribution = ', q{'}, $distribution, q{'}, "\n";

        # determine if any OS-specific dependencies exist for this distribution
        if ((exists $OS_SPECIFIC_DEPENDENCIES->{$distribution}) and (defined $OS_SPECIFIC_DEPENDENCIES->{$distribution})) {
            if ((exists $OS_SPECIFIC_DEPENDENCIES->{$distribution}->{$output_format}) and 
                (defined $OS_SPECIFIC_DEPENDENCIES->{$distribution}->{$output_format})) {
                $OS_SPECIFIC_DEPENDENCIES_DISTRIBUTION = $OS_SPECIFIC_DEPENDENCIES->{$distribution}->{$output_format};
            }
        }

print {*STDERR} 'in recurse(), have $OS_SPECIFIC_DEPENDENCIES_DISTRIBUTION = ', Dumper($OS_SPECIFIC_DEPENDENCIES_DISTRIBUTION);

        # skip dependencies contained in the Perl core, to avoid FPM error:
        # /usr/local/share/gems/gems/fpm-1.10.0/lib/fpm/package/cpan.rb:341:in `download': undefined method `[]' for nil:NilClass (NoMethodError)

        # SKIP CONDITION 0a. CURRENT MODULE ALREADY IN PERL CORE (via MetaCPAN::Client) AS SINGLE-LIFE
        # this identifies "single life" (in Perl core only, not on CPAN) core modules like 'Config' or 'File::Basename',
        # which should NOT be processed because they do not exist on CPAN independent of the latest perl-X.X.X.tar.gz distribution
        if ($distribution_cpan_file =~ m/^.*\/perl-[.\d]+\.tar\.gz$/) {
print {*STDERR} 'in recurse(), SKIPPING MODULE, ALREADY IN PERL CORE (via MetaCPAN::Client): ', q{'}, $input_module, q{'}, "\n";
            if ($distribution ne 'perl') {
                die 'CPANtoFPM ERROR, Distribution CPAN file indicates Perl core, but distribution is not ', q{'}, 'perl', q{'}, ': ', q{'}, $distribution, q{'}, ', dying';
            }

print {*STDERR} 'in recurse(), about to call update_dependencies_flat(), perl_core_cpan_shell...', "\n";
            update_dependencies_flat($dependencies_flat, $dependencies_flat->{_stack_master}, $input_module, $distribution);  # $distribution is 'perl'
print {*STDERR} 'in recurse(), ret from call to update_dependencies_flat(), perl_core_cpan_shell, have updated $dependencies_flat = ', Dumper($dependencies_flat);

            # there are no already-processed subdeps for modules in the Perl core, they should not have any dependencies outside the Perl core!

            return { _skipped => 'perl_core_cpan_shell', _distribution => $distribution };  # return empty $dependencies, $distribution is 'perl'
        }
        # else
        # this identifies "dual life" (in Perl core and on CPAN) core modules, 
        # which SHOULD be processed in order to get the newest version from CPAN, as well as be present in the OS package management dependency system
    }
    else {
        # SKIP CONDITION 0b. CURRENT MODULE ALREADY IN PERL CORE (via Module::CoreList) AS SINGLE-LIFE
        # Module::CoreList methodology, only use as fall-back when MetaCPAN::Client returns undef;
        # this identifies "single life" (in Perl core only, not on CPAN) core modules like 'Config' or 'File::Basename',
        # which should NOT be processed because they do not exist on CPAN independent of the latest perl-X.X.X.tar.gz distribution
        if (Module::CoreList::is_core($input_module)) {
print {*STDERR} 'in recurse(), SKIPPING MODULE, ALREADY IN PERL CORE (via Module::CoreList): ', $input_module, "\n";

print {*STDERR} 'in recurse(), about to call update_dependencies_flat(), perl_core_module_corelist...', "\n";
            update_dependencies_flat($dependencies_flat, $dependencies_flat->{_stack_master}, $input_module, 'perl');  # hard-code 'perl' distribution, assumed because is_core()
print {*STDERR} 'in recurse(), ret from call to update_dependencies_flat(), have updated $dependencies_flat = ', Dumper($dependencies_flat);

            # there are no already-processed subdeps for modules in the Perl core, they should not have any dependencies outside the Perl core!

            return { _skipped => 'perl_core_module_corelist', _distribution => 'perl' };  # return empty $dependencies
        }
        else {
            die 'CPANtoFPM ERROR, Unable to determine CPAN distribution of input module: ', q{'}, $input_module, q{'}, ', dying';
        }
    }

    # SKIP CONDITION 1a. CURRENT MODULE MATCHES ON-STACK MODULE  (slow CPAN lookup, slow check via foreach)
    # skip dependencies still on the stack, to avoid circular dependency loops and/or incomplete $dependencies_flat; module processed under its own name
    my boolean $stack_match_found = 0;
    # construct a partial stack consisting of all modules which will need receive subdeps after this input module has fully completed processing
    my string_hashref $stack_partial = [];
    foreach my string_hashref $module_distribution_stack (@{$dependencies_flat->{_stack_master}}) {
        if ($stack_match_found) {
            # all entries on the stack after (AKA more recently than) the match will need to receive subdeps
            push @{$stack_partial}, $module_distribution_stack;
        }
        elsif ($input_module eq $module_distribution_stack->{module}) {
print {*STDERR} 'in recurse(), SKIPPING MODULE, STILL ON STACK: ', q{'}, $input_module, q{'}, "\n";
            if ($distribution ne $module_distribution_stack->{distribution}) {
                die 'CPANtoFPM ERROR, Current module ', q{'}, $input_module, q{'}, ' found on the stack, but current distribution ', q{'}, $distribution, q{'}, ' does not match on-stack distribution ', q{'}, $module_distribution_stack->{distribution}, q{'}, ', dying';
            }

            $stack_match_found = 1;
            # don't include current distribution in partial stack, will not need to receive subdeps
        }
    }
    if ($stack_match_found) {
print {*STDERR} 'in recurse(), have $stack_partial = ', Dumper($stack_partial), "\n";
        $dependencies_flat->{_stacks_partial}->{$input_module} = $stack_partial;
        return { _skipped => 'on_stack_module', _distribution => $distribution };  # return empty $dependencies
    }

    # SKIP CONDITION 1b. CURRENT MODULE MATCHES ALREADY-PROCESSED MODULE (slow CPAN lookup, fast check via exists)
    # skip dependencies already in the flattened hash, to avoid repeating the same packaging process; module processed under its own name
    if (exists $dependencies_flat->{$input_module}) {
print {*STDERR} 'in recurse(), SKIPPING MODULE, ALREADY PROCESSED: ', q{'}, $input_module, q{'}, "\n";
        if ($distribution ne $dependencies_flat->{$input_module}->{_distribution}) {
            die 'CPANtoFPM ERROR, Current module ', q{'}, $input_module, q{'}, ' already processed, but current distribution ', q{'}, $distribution, q{'}, ' does not match already-processed distribution ', q{'}, $dependencies_flat->{$input_module}->{_distribution}, q{'}, ', dying';
        }

print {*STDERR} 'in recurse(), about to call update_dependencies_flat(), already_processed_module...', "\n";
        update_dependencies_flat($dependencies_flat, $dependencies_flat->{_stack_master}, $input_module, $dependencies_flat->{$input_module}->{_distribution});
print {*STDERR} 'in recurse(), ret from call to update_dependencies_flat(), already_processed_module, have updated $dependencies_flat = ', Dumper($dependencies_flat);

        # must add all already-processed subdeps
        foreach my string $subdependency (keys %{$dependencies_flat->{$input_module}}) {
            next if ((substr $subdependency, 0, 1) eq '_');  # skip special variables, not actual modules
print {*STDERR} 'in recurse(), about to call update_dependencies_flat(), already_processed_module foreach()...', "\n";
#            update_dependencies_flat($dependencies_flat, $dependencies_flat->{_stack_master}, $subdependency, $dependencies_flat->{$subdependency}->{_distribution});  # UNDEFINED SOMETIMES
            update_dependencies_flat($dependencies_flat, $dependencies_flat->{_stack_master}, $subdependency, $dependencies_flat->{$input_module}->{$subdependency}->{distribution});
print {*STDERR} 'in recurse(), ret from call to update_dependencies_flat(), already_processed_module foreach(), have updated $dependencies_flat = ', Dumper($dependencies_flat);
        }

        return { _skipped => 'already_processed_module', _distribution => $distribution };  # return empty $dependencies
    }

    # SKIP CONDITION 2a. CURRENT DISTRIBUTION MATCHES ON-STACK MODULE (slow CPAN lookup, slow check via foreach)
    # skip dependencies still on the stack, to avoid circular dependency loops and/or incomplete $dependencies_flat; module processed as part of distribution under distribution's name
    $stack_match_found = 0;
    # construct a partial stack consisting of all modules which will need receive subdeps after this input module has fully completed processing
    $stack_partial = [];
    foreach my string_hashref $module_distribution_stack (@{$dependencies_flat->{_stack_master}}) {
        if ($stack_match_found) {
            # all entries on the stack after (AKA more recently than) the match will need to receive subdeps
            push @{$stack_partial}, $module_distribution_stack;
        }
        elsif ($distribution eq $module_distribution_stack->{module}) {
print {*STDERR} 'in recurse(), SKIPPING MODULE: ', q{'}, $input_module, q{'}, '; STILL ON STACK AS PART OF DISTRIBUTION: ', q{'}, $distribution, q{'}, "\n";
            if ($distribution ne $module_distribution_stack->{distribution}) {
                die 'CPANtoFPM ERROR, Current distribution ', q{'}, $distribution, q{'}, ' found as module on stack, but does not match on-stack distribution ', q{'}, $module_distribution_stack->{distribution}, q{'}, ', dying';
            }

            $stack_match_found = 1;
            # don't include current distribution in partial stack, will not need to receive subdeps
        }
    }
    if ($stack_match_found) {
print {*STDERR} 'in recurse(), have $stack_partial = ', Dumper($stack_partial), "\n";
        $dependencies_flat->{_stacks_partial}->{$input_module} = $stack_partial;
        return { _skipped => 'on_stack_distribution', _distribution => $distribution };  # return empty $dependencies
    }

    # SKIP CONDITION 2b. CURRENT DISTRIBUTION MATCHES ALREADY-PROCESSED MODULE  (slow CPAN lookup, fast check via exists)
    # skip dependencies already in the flattened hash, to avoid repeating the same packaging process; module processed as part of distribution under distribution's name
    if (exists $dependencies_flat->{$distribution}) {
print {*STDERR} 'in recurse(), SKIPPING MODULE: ', q{'}, $input_module, q{'}, '; ALREADY PROCESSED AS PART OF DISTRIBUTION: ', q{'}, $distribution, q{'}, "\n";
        if ($distribution ne $dependencies_flat->{$distribution}->{_distribution}) {
            die 'CPANtoFPM ERROR, Current distribution ', q{'}, $distribution, q{'}, ' already processed as module, but does not match already-processed distribution ', q{'}, $dependencies_flat->{$distribution}->{_distribution}, q{'}, ', dying';
        }

print {*STDERR} 'in recurse(), about to call update_dependencies_flat(), already_processed_distribution...', "\n";
        update_dependencies_flat($dependencies_flat, $dependencies_flat->{_stack_master}, $input_module, $distribution);
print {*STDERR} 'in recurse(), ret from call to update_dependencies_flat(), already_processed_distribution, have updated $dependencies_flat = ', Dumper($dependencies_flat);

        # must add all already-processed subdeps
        foreach my string $subdependency (keys %{$dependencies_flat->{$distribution}}) {
            next if ((substr $subdependency, 0, 1) eq '_');  # skip special variables, not actual modules
print {*STDERR} 'in recurse(), about to call update_dependencies_flat(), already_processed_distribution foreach()...', "\n";
#            update_dependencies_flat($dependencies_flat, $dependencies_flat->{_stack_master}, $subdependency, $dependencies_flat->{$subdependency}->{_distribution});  # UNDEFINED SOMETIMES???
            update_dependencies_flat($dependencies_flat, $dependencies_flat->{_stack_master}, $subdependency, $dependencies_flat->{$distribution}->{$subdependency}->{distribution});
print {*STDERR} 'in recurse(), ret from call to update_dependencies_flat(), already_processed_distribution foreach(), have updated $dependencies_flat = ', Dumper($dependencies_flat);
        }

        return { _skipped => 'already_processed_distribution', _distribution => $distribution };  # return empty $dependencies
    }

    # SKIP CONDITION 3a. CURRENT DISTRIBUTION MATCHES ON-STACK DISTRIBUTION  (slow CPAN lookup, slow check via foreach)
    # skip dependencies still on the stack, to avoid circular dependency loops and/or incomplete $dependencies_flat; module processed as part of distribution under a sibling module's name
    $stack_match_found = 0;
    # construct a partial stack consisting of all modules which will need receive subdeps after this input module has fully completed processing
    $stack_partial = [];
    foreach my string_hashref $module_distribution_stack (@{$dependencies_flat->{_stack_master}}) {
        if ($stack_match_found) {
            # all entries on the stack after (AKA more recently than) the match will need to receive subdeps
            push @{$stack_partial}, $module_distribution_stack;
        }
        elsif ($distribution eq $module_distribution_stack->{distribution}) {
print {*STDERR} 'in recurse(), SKIPPING MODULE: ', q{'}, $input_module, q{'}, '; STILL ON STACK AS PART OF DISTRIBUTION: ', q{'}, $distribution, q{'}, '; DUE TO SIBLING MODULE: ', q{'}, $module_distribution_stack->{distribution}, q{'}, "\n";
            $stack_match_found = 1;
            # don't include current distribution in partial stack, will not need to receive subdeps
        }
    }
    if ($stack_match_found) {
print {*STDERR} 'in recurse(), have $stack_partial = ', Dumper($stack_partial), "\n";
        $dependencies_flat->{_stacks_partial}->{$input_module} = $stack_partial;
        return { _skipped => 'on_stack_sibling', _distribution => $distribution };  # return empty $dependencies
    }

    # SKIP CONDITION 3b. CURRENT DISTRIBUTION MATCHES ALREADY-PROCESSED DISTRIBUTION  (slow CPAN lookup, slow check via foreach)
    # loop through all dependency modules to check if any of them are the current module's siblings (belonging to same distribution)
    foreach my string $module_processed_already (sort keys %{$dependencies_flat}) {
        next if ((substr $module_processed_already, 0, 1) eq '_');  # skip special variables, not actual modules
print {*STDERR} 'in recurse(), top of sibling module foreach() loop, have $module_processed_already = ', q{'}, $module_processed_already, q{'}, "\n";
        my string $distribution_processed_already = $dependencies_flat->{$module_processed_already}->{_distribution};
print {*STDERR} 'in recurse(), in sibling module foreach() loop, have $distribution_processed_already = ', q{'}, $distribution_processed_already, q{'}, "\n";

        # skip dependencies already in the flattened hash, to avoid repeating the same packaging process; module processed as part of distribution under a sibling module's name
        if ($distribution eq $distribution_processed_already) {
print {*STDERR} 'in recurse(), SKIPPING MODULE: ', q{'}, $input_module, q{'}, '; ALREADY PROCESSED AS PART OF DISTRIBUTION: ', q{'}, $distribution, q{'}, '; DUE TO SIBLING MODULE: ', q{'}, $module_processed_already, q{'}, "\n";

print {*STDERR} 'in recurse(), about to call update_dependencies_flat(), already_processed_sibling...', "\n";
            update_dependencies_flat($dependencies_flat, $dependencies_flat->{_stack_master}, $input_module, $distribution);
print {*STDERR} 'in recurse(), ret from call to update_dependencies_flat(), already_processed_sibling, have updated $dependencies_flat = ', Dumper($dependencies_flat);

            # must add all already-processed subdeps
            foreach my string $subdependency (keys %{$dependencies_flat->{$module_processed_already}}) {
                next if ((substr $subdependency, 0, 1) eq '_');  # skip special variables, not actual modules
print {*STDERR} 'in recurse(), about to call update_dependencies_flat(), already_processed_sibling foreach()...', "\n";
#                update_dependencies_flat($dependencies_flat, $dependencies_flat->{_stack_master}, $subdependency, $distribution);  # WRONG VALUE
#                update_dependencies_flat($dependencies_flat, $dependencies_flat->{_stack_master}, $subdependency, $dependencies_flat->{$subdependency}->{_distribution});  # UNDEFINED SOMETIMES?
                update_dependencies_flat($dependencies_flat, $dependencies_flat->{_stack_master}, $subdependency, $dependencies_flat->{$module_processed_already}->{$subdependency}->{distribution});
print {*STDERR} 'in recurse(), ret from call to update_dependencies_flat(), already_processed_sibling foreach(), have updated $dependencies_flat = ', Dumper($dependencies_flat);
            }

            return { _skipped => 'already_processed_sibling', _distribution => $distribution };  # return empty $dependencies
        }
    }

# DEV NOTE: the following debugging statement causes auto-vivification of $dependencies_flat elements, which may trigger false positive for 'SKIPPING DISTRIBUTION, ALREADY PROCESSED' above, moved here below but may still not be safe to use
####print {*STDERR} 'in recurse(), have scalar keys %{$dependencies_flat->{$dependencies_flat->{_module_master}}} = ', scalar keys %{$dependencies_flat->{$dependencies_flat->{_module_master}}}, "\n";


# THEN START HERE: must also check for spec & srpc & deps files of already-built distros, load deps from file for use when creating final tarball
# THEN START HERE: must also check for spec & srpc & deps files of already-built distros, load deps from file for use when creating final tarball
# THEN START HERE: must also check for spec & srpc & deps files of already-built distros, load deps from file for use when creating final tarball


    # skip distributions which have packages already on disk, to avoid repeating the same packaging process; presumably they were built by cpantofpm
    opendir(my filehandleref $CURRENT_DIR, ".")
        or die 'CPANtoFPM ERROR, Can not open current directory for reading a list of files;', "\n", $OS_ERROR, "\n", 'dying';
    my string_arrayref $current_dir_files = [grep(/\.$output_format$/, readdir($CURRENT_DIR))];
    closedir($CURRENT_DIR)
        or die 'CPANtoFPM ERROR, Can not close current directory after reading a list of files;', "\n", $OS_ERROR, "\n", 'dying';
    foreach my string $current_dir_file (@{$current_dir_files}) {
        # DEB, START HERE: make sure following regex also matches deb files
        # DEB, START HERE: make sure following regex also matches deb files
        # DEB, START HERE: make sure following regex also matches deb files
 
        # HARD-CODED EXAMPLES:
        # perl-PathTools-3.74-1.x86_64.rpm
        # perl-BSON-v1.6.7-1.noarch.rpm
        if ($current_dir_file =~ m/^.*perl-$distribution-[v.\d]+-\w+\.$output_format$/) {
print {*STDERR} 'in recurse(), SKIPPING MODULE, DISTRIBUTION ALREADY PROCESSED & SAVED DURING PRIOR EXECUTION: ', q{'}, $current_dir_file, q{'}, "\n";

# THEN START HERE: don't die, need load deps file & call update_dependencies_flat()
# THEN START HERE: don't die, need load deps file & call update_dependencies_flat()
# THEN START HERE: don't die, need load deps file & call update_dependencies_flat()

            die 'CPANtoFPM ERROR, Can not load deps file, not yet implemented, dying';

            return { _skipped => 'already_processed_distribution_prior_execution', _distribution => $distribution };  # return empty $dependencies
        }
    }

    # NEED ANSWER: keep EPEL checks or remove?!?
    # NEED ANSWER: keep EPEL checks or remove?!?
    # NEED ANSWER: keep EPEL checks or remove?!?

    # NEED ADD: check to ensure @base and @epel are both enabled?
    # NEED ADD: check to ensure @base and @epel are both enabled?
    # NEED ADD: check to ensure @base and @epel are both enabled?

=DISABLED, incorrect code, do rebuild base & epel packages
    # skip modules which already exist in the official CentOS repo or the semi-official EPEL repo
    # for RPM format, Perl module 'IO::Compress::Gzip' is packaged as 'perl(IO::Compress::Gzip)' 
    my string $module_package_name = 'perl(' . $input_module . ')';
    my string_arrayref $module_repos = run_yum_query($module_package_name);
    foreach my string $module_repo (@{$module_repos}) {
        if (($module_repo eq  'base') or 
            ($module_repo eq '@base') or 
            ($module_repo eq  'epel') or 
            ($module_repo eq '@epel')) {
print {*STDERR} 'in recurse(), SKIPPING MODULE, ALREADY REPOSITED IN THE FOLLOWING REPO(S): ', (join ',', @{$module_repos}), "\n";
            return { _skipped => 'already_reposited_module', _distribution => $distribution };  # return empty $dependencies
        }
    }    

    # skip distributions which already exist in the official CentOS repo or the semi-official EPEL repo
    # for RPM format, Perl distribution 'IO::Compress' is packaged as 'perl-IO-Compress' 
    my string $distribution_package_name = $distribution;
    $distribution_package_name =~ s/::/-/gxms;
    $distribution_package_name = 'perl-' . $distribution_package_name;
    my string_arrayref $distribution_repos = run_yum_query($distribution_package_name);
    foreach my string $distribution_repo (@{$distribution_repos}) {
        if (($distribution_repo eq  'base') or 
            ($distribution_repo eq '@base') or 
            ($distribution_repo eq  'epel') or 
            ($distribution_repo eq '@epel')) {
print {*STDERR} 'in recurse(), SKIPPING MODULE, DISTRIBUTION ALREADY REPOSITED IN THE FOLLOWING REPO(S): ', (join ',', @{$distribution_repos}), "\n";
        return { _skipped => 'already_reposited_distribution', _distribution => $distribution };  # return empty $dependencies
        }
    } 
=cut

    # [[[ CALL DAISY-CHAINED SUBROUTINES ]]]
    my string_hashref_hashref $dependencies;
    if ($output_format eq 'deb') {
###print {*STDERR} 'in recurse(), about to call determine_control_file_dependencies(determine_control_file(run_fpm()))...', "\n";
        # DEV NOTE: process dependencies by module
        $dependencies = determine_control_file_dependencies(determine_control_file(run_fpm($input_module, $output_format, $version_template, $work_dir, $OS_SPECIFIC_DEPENDENCIES_DISTRIBUTION)));
    }
    elsif ($output_format eq 'rpm') {
print {*STDERR} 'in recurse(), about to call daisy-chain determine_spec_file_dependencies(save_rpm_srpm_spec(run_fpm()))...', "\n";
print {*STDERR} "\n\n", '----daisy-chain-----' x 5, "\n";
print {*STDERR}         'v' x 100, "\n\n";
        # DEV NOTE: process dependencies by module
        $dependencies = determine_spec_file_dependencies(save_rpm_srpm_spec(run_fpm($input_module, $output_format, $version_template, $work_dir, $OS_SPECIFIC_DEPENDENCIES_DISTRIBUTION)));
print {*STDERR} "\n\n", '^' x 100, "\n";
print {*STDERR}         '----daisy-chain-----' x 5, "\n\n";
print {*STDERR} 'in recurse(), ret from call to daisy-chain determine_spec_file_dependencies(save_rpm_srpm_spec(run_fpm()))', "\n";
    }
    else {
        die q{CPANtoFPM ERROR, Invalid output package format '} . $output_format . q{' specified, must be either 'deb' or 'rpm', dying}, "\n";
    }

    # save distribution info with each module in the nested dependencies
    $dependencies->{_distribution} = $distribution;
#print {*STDERR} 'in recurse(), have $dependencies = ', Dumper($dependencies), "\n";

    # update arch based on this module
print {*STDERR} 'in recurse(), after daisy-chain on $distribution = ', q{'}, $distribution, q{'}, ', about to call update_arch() w/ $arch_master = ', q{'}, $arch_master, q{'}, '...', "\n";
    $arch_master = update_arch($arch_master, $dependencies->{_package_info}->{arch});
    $dependencies->{_arch_master} = $arch_master;  # store possibly-updated $arch_master into _arch_master, to be returned as part of $dependencies
print {*STDERR} 'in recurse(), after daisy-chain on $distribution = ', q{'}, $distribution, q{'}, ', ret from call to update_arch() w/ $arch_master = ', q{'}, $arch_master, q{'}, '...', "\n";

print {*STDERR} 'in recurse(), have pre-update $dependencies_flat->{_stack_master} = ', Dumper($dependencies_flat->{_stack_master});
print {*STDERR} 'in recurse(), about to call update_dependencies_flat(), not skipping...', "\n";
    update_dependencies_flat($dependencies_flat, $dependencies_flat->{_stack_master}, $input_module, $distribution);
print {*STDERR} 'in recurse(), ret from call to update_dependencies_flat(), have updated $dependencies_flat = ', Dumper($dependencies_flat);
#print {*STDERR} 'in recurse(), have updated $dependencies_flat->{_dependencies_flat_master_module} = ', Dumper($dependencies_flat->{_dependencies_flat_master_module});
# DEV NOTE: the following debugging statement may also cause auto-vivification of $dependencies_flat elements, disabled for now
###print {*STDERR} 'in recurse(), have updated scalar keys %{$dependencies_flat->{$dependencies_flat->{_module_master}}} = ', scalar keys %{$dependencies_flat->{$dependencies_flat->{_module_master}}}, "\n";

=DISABLED, TMP DEBUG
    foreach my string $dependency_module (sort keys %{$dependencies}) {
        next if ((substr $dependency_module, 0, 1) eq '_');  # skip special variables, not actual modules
print {*STDERR} '[[[ DEBUG ]]] in recurse(), top of dependency debug loop, have $dependency_module = ', q{'}, $dependency_module, q{'}, "\n";
        if (exists $dependencies->{$dependency_module}->{_version}) {
print {*STDERR} '[[[ DEBUG ]]] in recurse(), dependency debug loop, have $dependencies->{$dependency_module}->{_version} = ', q{'}, $dependencies->{$dependency_module}->{_version}, q{'}, "\n";
        }
        else {
print {*STDERR} '[[[ DEBUG ]]] in recurse(), dependency debug loop, have $dependencies->{$dependency_module}->{_version} = <<< DOES NOT EXIST >>>', "\n";
        }
        if (exists $dependencies->{$dependency_module}->{_min_version}) {
print {*STDERR} '[[[ DEBUG ]]] in recurse(), dependency debug loop, have $dependencies->{$dependency_module}->{_min_version} = ', q{'}, $dependencies->{$dependency_module}->{_min_version}, q{'}, "\n";
        }
        else {
print {*STDERR} '[[[ DEBUG ]]] in recurse(), dependency debug loop, have $dependencies->{$dependency_module}->{_min_version} = <<< DOES NOT EXIST >>>', "\n";
        }
    }
=cut

    # depth-first recurse for each dependency, in order to process subdependencies
    foreach my string $dependency_module (sort keys %{$dependencies}) {
print {*STDERR} ('------recurse-loop-------' x 2), "\n";
print {*STDERR} ('v' x 50), "\n";

        next if ((substr $dependency_module, 0, 1) eq '_');  # skip special variables, not actual modules
print {*STDERR} 'in recurse(), top of dependency loop on $distribution = ', q{'}, $distribution, q{'}, ', have $dependency_module = ', q{'}, $dependency_module, q{'}, "\n";

        my boolean $is_valid_version = 1;
        my boolean $is_valid_min_version = 1;
        my boolean $is_valid_cpan_version = 1;
        my boolean $versions_tested = -1;
        

        # check versions
        if (exists $dependencies->{$dependency_module}->{_version}) {
            my $module_object = CPAN::Shell->expand('Module', $dependency_module);
            if (defined $module_object) {
                my number $cpan_version = $module_object->cpan_version();
print {*STDERR} 'in recurse(), have $dependencies->{$dependency_module}->{_version} = ', q{'}, $dependencies->{$dependency_module}->{_version}, q{'}, "\n";

                # [[[ BEGIN DUPLICATE CODE, TEST FOR VALID VERSION ]]]
                # use version::is_lax() to test if version is valid and can be passed to version->parse()
                if (not version::is_lax($dependencies->{$dependency_module}->{_version})) {
print {*STDERR} 'in recurse(), in dependency loop on $distribution = ', q{'}, $distribution, q{'}, ', invalid version ', q{'}, $dependencies->{$dependency_module}->{_version}, q{'}, ', trying to strip underscores...', "\n";
                    # if not a valid version, try stripping underscores
                    $dependencies->{$dependency_module}->{_version} =~ s/_//gxms;
                    if (not version::is_lax($dependencies->{$dependency_module}->{_version})) {
print {*STDERR} 'in recurse(), in dependency loop on $distribution = ', q{'}, $distribution, q{'}, ', totally invalid version ', q{'}, $dependencies->{$dependency_module}->{_version}, q{'}, ', giving up!', "\n";
                        # if still not a valid version, mark as invalid and do not parse
                        $is_valid_version = 0;
                    }
                }
                # [[[ END DUPLICATE CODE, TEST FOR VALID VERSION ]]]

                # [[[ BEGIN DUPLICATE CODE, TEST FOR VALID VERSION ]]]
                # use version::is_lax() to test if version is valid and can be passed to version->parse()
                if (not version::is_lax($cpan_version)) {
print {*STDERR} 'in recurse(), in dependency loop on $distribution = ', q{'}, $distribution, q{'}, ', invalid CPAN version ', q{'}, $cpan_version, q{'}, ', trying to strip underscores...', "\n";
                    # if not a valid version, try stripping underscores
                    $cpan_version =~ s/_//gxms;
                    if (not version::is_lax($cpan_version)) {
print {*STDERR} 'in recurse(), in dependency loop on $distribution = ', q{'}, $distribution, q{'}, ', totally invalid CPAN version ', q{'}, $cpan_version, q{'}, ', giving up!', "\n";
                        # if still not a valid version, mark as invalid and do not parse
                        $is_valid_cpan_version = 0;
                    }
                }
                # [[[ END DUPLICATE CODE, TEST FOR VALID VERSION ]]]

                if (not $is_valid_version) {
                    print 'CPANtoFPM WARNING, Valid Version Not Found: Distribution ', $distribution, ' requires ', $dependency_module, ' version ', 
                        q{'}, $dependencies->{$dependency_module}->{_version}, q{'}, ', but can not parse via version->parse(), will try min_version instead if present';
                }
                if (not $is_valid_cpan_version) {
                    print 'CPANtoFPM WARNING, Valid CPAN Version Not Found: Distribution ', $distribution, ' requires ', $dependency_module, ', version ', 
                        q{'}, $cpan_version, q{'}, ' found of CPAN, but can not parse via version->parse(), will try min_version instead if present';
                }
                if ($is_valid_version and $is_valid_cpan_version) {
                    # NEED UPGRADE: handle check for the existence of a specific prior version of a module
#                    if ($cpan_version != $dependencies->{$dependency_module}->{_version}) {  # possibly wrong, numeric comparison instead of version
                    if (version->parse($cpan_version) != version->parse($dependencies->{$dependency_module}->{_version})) {
                        die 'CPANtoFPM ERROR, Exact Version Not Found: ', $dependency_module, ' ', $dependencies->{$dependency_module}->{_version},
                            ' required, but ', $cpan_version, ' found on CPAN, NEED UPGRADE TO HANDLE EXACT VERSIONS, dying';
                    }
                    $versions_tested = 1;
                }
                else {
                    $versions_tested = 0;
                }
            }
            else {
                    die 'CPANtoFPM ERROR, Exact Version Not Found: ', $dependency_module, ' ', $dependencies->{$dependency_module}->{_version},
                        ' required, but no version found on CPAN, dying';
            }
        }

        # don't try to test min_version if version already tested above
        if ((not ($versions_tested == 1)) and (exists $dependencies->{$dependency_module}->{_min_version})) {
            my $module_object = CPAN::Shell->expand('Module', $dependency_module);
            if (defined $module_object) {
                my number $cpan_version = $module_object->cpan_version();
print {*STDERR} 'in recurse(), have $dependencies->{$dependency_module}->{_min_version} = ', q{'}, $dependencies->{$dependency_module}->{_min_version}, q{'}, "\n";

                # [[[ BEGIN DUPLICATE CODE, TEST FOR VALID VERSION ]]]
                # use version::is_lax() to test if version is valid and can be passed to version->parse()
                if (not version::is_lax($dependencies->{$dependency_module}->{_min_version})) {
print {*STDERR} 'in recurse(), in dependency loop on $distribution = ', q{'}, $distribution, q{'}, ', invalid min_version ', q{'}, $dependencies->{$dependency_module}->{_min_version}, q{'}, ', trying to strip underscores...', "\n";
                    # if not a valid version, try stripping underscores
                    $dependencies->{$dependency_module}->{_min_version} =~ s/_//gxms;
                    if (not version::is_lax($dependencies->{$dependency_module}->{_min_version})) {
print {*STDERR} 'in recurse(), in dependency loop on $distribution = ', q{'}, $distribution, q{'}, ', totally invalid min_version ', q{'}, $dependencies->{$dependency_module}->{_min_version}, q{'}, ', giving up!', "\n";
                        # if still not a valid version, mark as invalid and do not parse
                        $is_valid_min_version = 0;
                    }
                }
                # [[[ END DUPLICATE CODE, TEST FOR VALID VERSION ]]]

                # [[[ BEGIN DUPLICATE CODE, TEST FOR VALID VERSION ]]]
                # use version::is_lax() to test if version is valid and can be passed to version->parse()
                if (not version::is_lax($cpan_version)) {
print {*STDERR} 'in recurse(), in dependency loop on $distribution = ', q{'}, $distribution, q{'}, ', invalid CPAN version ', q{'}, $cpan_version, q{'}, ', trying to strip underscores...', "\n";
                    # if not a valid version, try stripping underscores
                    $cpan_version =~ s/_//gxms;
                    $is_valid_cpan_version = 1;  # reset due to use in checking version above
                    if (not version::is_lax($cpan_version)) {
print {*STDERR} 'in recurse(), in dependency loop on $distribution = ', q{'}, $distribution, q{'}, ', totally invalid CPAN version ', q{'}, $cpan_version, q{'}, ', giving up!', "\n";
                        # if still not a valid version, mark as invalid and do not parse
                        $is_valid_cpan_version = 0;
                    }
                }
                # [[[ END DUPLICATE CODE, TEST FOR VALID VERSION ]]]

                if (not $is_valid_min_version) {
                    print 'CPANtoFPM WARNING, Valid Minimum Version Not Found: Distribution ', $distribution, ' requires ', $dependency_module, ' min_version ', 
                        q{'}, $dependencies->{$dependency_module}->{_min_version}, q{'}, ', but can not parse via version->parse(), giving up!';
                }
                if (not $is_valid_cpan_version) {
                    print 'CPANtoFPM WARNING, Valid CPAN Version Not Found: Distribution ', $distribution, ' requires ', $dependency_module, ', version ', 
                        q{'}, $cpan_version, q{'}, ' found of CPAN, but can not parse via version->parse(), giving up!';
                }
                if ($is_valid_min_version and $is_valid_cpan_version) {
#                    if ($cpan_version < $dependencies->{$dependency_module}->{_min_version}) {  # possibly wrong, numeric comparison instead of version
                    if (version->parse($cpan_version) < version->parse($dependencies->{$dependency_module}->{_min_version})) {
                        die 'CPANtoFPM ERROR, Minimum Version Not Found: ', $dependency_module, ' ', $dependencies->{$dependency_module}->{_min_version},
                            ' required, but ', $cpan_version, ' found on CPAN, dying';
                    }
                    $versions_tested = 1;
                }
                else {
                    $versions_tested = 0;
                }
            }
            else {
                die 'CPANtoFPM ERROR, Minimum Version Not Found: ', $dependency_module, ' ', $dependencies->{$dependency_module}->{_min_version},
                    ' required, but no version found on CPAN, dying';
            }
        }

        if ($versions_tested == 0) {
            die 'CPANtoFPM ERROR, No Valid Version Or Minimum Version Not Found: ', $dependency_module, ' has a version and/or min_version, but neither can be parsed, dying';
        }

        # set version_template from subdependency version requirements if present,
        # ignore versions w/out any dot '.' characters because they do not serve as a usable template 
        my string $dependency_version_template = q{};

        # if _version exists and contains at least one dot '.' character, then use it as version_template
        if ((exists $dependencies->{$dependency_module}->{_version}) and
            ($dependencies->{$dependency_module}->{_version} =~ tr/\.//)) {
            $dependency_version_template = $dependencies->{$dependency_module}->{_version};
        }
        # elsif _min_version exists and contains at least one dot '.' character, then use it as version_template
        if ((exists $dependencies->{$dependency_module}->{_min_version}) and
            ($dependencies->{$dependency_module}->{_min_version} =~ tr/\.//)) {
            $dependency_version_template = $dependencies->{$dependency_module}->{_min_version};
        }

        # [[[ RECURSE ]]]
print {*STDERR} 'in recurse(), have $dependency_module = ', q{'}, $dependency_module, q{'}, ', about to call recurse()...', "\n";
print {*STDERR} "\n\n", '====recurse====' x 10, "\n";
print {*STDERR}         'v' x 150, "\n\n";
        push @{$dependencies_flat->{_stack_master}}, {module => $input_module, distribution => $distribution};
        my string_hashref_hashref $subdependencies = recurse($dependency_module, $output_format, $dependency_version_template, $work_dir, $dependencies_flat, $arch_master, $OS_SPECIFIC_DEPENDENCIES);
        pop @{$dependencies_flat->{_stack_master}};
print {*STDERR} "\n\n", '^' x 150, "\n";
print {*STDERR}         '====recurse====' x 10, "\n\n";
print {*STDERR} 'in recurse(), have $dependency_module = ', q{'}, $dependency_module, q{'}, ', ret from call to recurse()...', "\n";
print {*STDERR} 'in recurse(), have $subdependencies = ', Dumper($subdependencies);

        if (not defined $subdependencies) {
            die 'CPANtoFPM ERROR, Subdependencies Not Correctly Determined: ', q{'}, $dependency_module, q{'}, "\n", 'dying';
        }

        # process _stacks_partial if just-processed $dependency_module is on it
        if ((exists $dependencies_flat->{_stacks_partial}) and (defined $dependencies_flat->{_stacks_partial})) {
print {*STDERR} 'in recurse(), after recurse on $dependency_module = ', q{'}, $dependency_module, q{'}, ', have $dependencies_flat->{_stacks_partial} = ', Dumper($dependencies_flat->{_stacks_partial});
            if (exists $dependencies_flat->{_stacks_partial}->{$dependency_module}) {
print {*STDERR} 'in recurse(), after recurse on $dependency_module = ', q{'}, $dependency_module, q{'}, ', STACK PARTIAL, about to call update_dependencies_flat()...', "\n";
                update_dependencies_flat($dependencies_flat, $dependencies_flat->{_stacks_partial}->{$dependency_module}, $dependency_module, $subdependencies->{_distribution});
print {*STDERR} 'in recurse(), STACK PARTIAL, ret from call to update_dependencies_flat(), have updated $dependencies_flat = ', Dumper($dependencies_flat);
            }
        }

        if (not exists $subdependencies->{_skipped}) {
            # DEV NOTE: do NOT call save_deps_file() here, because if a skipped dependency is still on the stack (not finished processing) then you would be saving incomplete deps info
            # save subdependencies & distributions processed for this distribution, $subdependencies will be updated to include deps_file entries
#print {*STDERR} 'in recurse(), after recurse on $dependency_module = ', q{'}, $dependency_module, q{'}, ', about to call save_deps_file() w/ $subdependencies->{_distribution} = ', q{'}, $subdependencies->{_distribution}, q{'}, '...', "\n";
#            $subdependencies = save_deps_file($dependency_module, $subdependencies->{_distribution}, $subdependencies, $dependencies_flat, $dependencies);
#print {*STDERR} 'in recurse(), after recurse on $dependency_module = ', q{'}, $dependency_module, q{'}, ', ret from call to save_deps_file() w/ $subdependencies->{_distribution} = ', q{'}, $subdependencies->{_distribution}, q{'}, "\n";

            # update arch based on this subdependency
print {*STDERR} 'in recurse(), after recurse on $dependency_module = ', q{'}, $dependency_module, q{'}, ', about to call update_arch() w/ $arch_master = ', q{'}, $arch_master, q{'}, '...', "\n";
            $arch_master = update_arch($arch_master, $subdependencies->{_arch_master});
            $dependencies->{_arch_master} = $arch_master;  # store possibly-updated $arch_master back into _arch_master, to be returned as part of $dependencies
print {*STDERR} 'in recurse(), after recurse on $dependency_module = ', q{'}, $dependency_module, q{'}, ', ret from call to update_arch() w/ possibly-updated $arch_master = ', q{'}, $arch_master, q{'}, '...', "\n";
        }
        else {
#print {*STDERR} 'in recurse(), after recurse on $dependency_module = ', q{'}, $dependency_module, q{'}, ', SKIPPED _skipped module detected, not calling save_deps_file() or update_arch()', "\n";
print {*STDERR} 'in recurse(), after recurse on $dependency_module = ', q{'}, $dependency_module, q{'}, ', SKIPPED _skipped module detected, not calling update_arch()', "\n";
        }

print {*STDERR} 'in recurse(), after recurse on $dependency_module = ', q{'}, $dependency_module, q{'}, ', about to merge subdeps into existing deps', "\n";
        # merge subdeps into existing deps
        $dependencies->{$dependency_module} = {%{$dependencies->{$dependency_module}}, %{$subdependencies}};
    }

    return $dependencies;
}

# UPDATE FLATTENED HASHES OF DISTRIBUTIONS PROCESSED
# UPDATE FLATTENED HASHES OF DISTRIBUTIONS PROCESSED
# UPDATE FLATTENED HASHES OF DISTRIBUTIONS PROCESSED
sub update_dependencies_flat {
    { my void $RETURN_TYPE };
    (my string_hashref_hashref $dependencies_flat, my string_hashref_arrayref $stack, my string $module_processing_now, my string $distribution_processing_now) = @ARG;
print {*STDERR} 'in update_dependencies_flat(), received $stack = ', Dumper($stack);
print {*STDERR} 'in update_dependencies_flat(), received $module_processing_now = ', q{'}, $module_processing_now, q{'}, "\n";
print {*STDERR} 'in update_dependencies_flat(), received $distribution_processing_now = ', q{'}, $distribution_processing_now, q{'}, "\n";

    # add just-processed dependency into lists flattened dependencies $dependencies_flat before recursing, to avoid circular dependency loops;
    # use stack to add dependency to list of flattened dependencies for each calling distribution, not just one big global flattened list;
    # DEV NOTE: process dependencies by module
    foreach my string $module_distribution_on_stack (@{$stack}) {
print {*STDERR} 'in update_dependencies_flat(), top of foreach() loop, have $module_distribution_on_stack = ', Dumper($module_distribution_on_stack);
        # do not set a module as its own dependency
        if ($module_processing_now eq $module_distribution_on_stack->{module}) {
print {*STDERR} 'in update_dependencies_flat(), SKIPPING MODULE DUE TO CIRCULAR DEPENDENCY, will not make module its own dependency: ', q{'}, $module_processing_now, q{'}, "\n";
            next;
        }

        # create new dependency entries
        if (not exists $dependencies_flat->{$module_distribution_on_stack->{module}}) {
            $dependencies_flat->{$module_distribution_on_stack->{module}} = {};
        }
        if (not exists $dependencies_flat->{$module_distribution_on_stack->{module}}->{_distribution}) {
            $dependencies_flat->{$module_distribution_on_stack->{module}}->{_distribution} = $module_distribution_on_stack->{distribution};
        }
        if (not exists $dependencies_flat->{$module_distribution_on_stack->{module}}->{$module_processing_now}) {
            $dependencies_flat->{$module_distribution_on_stack->{module}}->{$module_processing_now} = {distribution => $distribution_processing_now, count => 0};
        }
        $dependencies_flat->{$module_distribution_on_stack->{module}}->{$module_processing_now}->{count} += 1;
    }
    return;
}

# UPDATE ARCHITECTURE
# UPDATE ARCHITECTURE
# UPDATE ARCHITECTURE
sub update_arch {
    { my string $RETURN_TYPE };
    ( my string $arch_master, my string $arch_new ) = @ARG;

print {*STDERR} 'in update_arch(), received $arch_master = ', q{'}, $arch_master, q{'}, "\n";
print {*STDERR} 'in update_arch(), have $arch_new = ', q{'}, $arch_new, q{'}, "\n";

    # prefer anything over the initially-empty $arch_master or 'noarch'
    if (($arch_master eq q{}) or ($arch_master eq 'noarch')) {
        $arch_master = $arch_new;
print {*STDERR} 'in update_arch(), have modified $arch_master = ', q{'}, $arch_master, q{'}, "\n";
    }
    elsif ($arch_new eq 'noarch') {
print {*STDERR} q{in update_arch(), $arch_master already set and $arch_new is 'noarch', have un-modified $arch_master = }, q{'}, $arch_master, q{'}, "\n";
    }
    elsif ($arch_master ne $arch_new) {
        die 'CPANtoFPM ERROR, Package architecture mis-match, found both ', q{'}, $arch_master, q{'}, ' and ', q{'}, $arch_new, q{'}, ', dying';
    }
    # $arch_master is already the same as $arch_new    
    else {
print {*STDERR} q{in update_arch(), $arch_master already set and matches $arch_new, have un-modified $arch_master = }, q{'}, $arch_master, q{'}, "\n";
    }

    return $arch_master;
}

# RUN YUM QUERY COMMAND
# RUN YUM QUERY COMMAND
# RUN YUM QUERY COMMAND
sub run_yum_query {
    { my string_arrayref $RETURN_TYPE };
    ( my string $module_or_distribution, my string $yum_config_file ) = @ARG;

# HARD-CODED EXAMPLES: 

# $ yum -q provides 'perl-IO-Compress'
#perl-IO-Compress-2.061-2.el7.noarch : Read and write compressed data
#Repo        : base
#  [[[ 3 BLANK LINES ]]]
#perl-IO-Compress-2.081-1.noarch : IO Interface to compressed data files/buffers
#Repo        : centos7-perl-cpan
#  [[[ 3 BLANK LINES ]]]
#perl-IO-Compress-2.061-2.el7.noarch : Read and write compressed data
#Repo        : @base

# $ yum -q provides 'perl(IO::Compress::Gzip)'
#perl-IO-Compress-2.061-2.el7.noarch : Read and write compressed data
#Repo        : base
#Matched from:
#Provides    : perl(IO::Compress::Gzip) = 2.061
#  [[[ 3 BLANK LINES ]]]
#perl-IO-Compress-2.081-1.noarch : IO Interface to compressed data files/buffers
#Repo        : centos7-perl-cpan
#Matched from:
#Provides    : perl(IO::Compress::Gzip) = 2.081
#  [[[ 3 BLANK LINES ]]]
#perl-IO-Compress-2.061-2.el7.noarch : Read and write compressed data
#Repo        : @base
#Matched from:
#Provides    : perl(IO::Compress::Gzip) = 2.061

# $ yum -q provides 'perl(FOO::INVALID)'
#  [[[ NO OUTPUT GENERATED ]]]

    my string $stdout_generated = q{};
    my string $stderr_generated = q{};

    # HARD-CODED EXAMPLE:
    # yum -q provides 'perl-IO-Compress'
    my string $execute_command = 'yum ';
    if ((defined $yum_config_file) and ($yum_config_file ne q{})) {
        $execute_command .= '-c ' . $yum_config_file . q{ };
    }
    $execute_command .= '-q provides ' . q{'} . $module_or_distribution . q{'};

#print {*STDERR} 'in run_yum_query(), have $execute_command = ' . $execute_command . "\n";
#print {*STDERR} 'in run_yum_query(), about to call run3()...' . "\n";

    run3( $execute_command, \undef, \$stdout_generated, \$stderr_generated );  # no simultaneous view & capture; child STDIN from /dev/null, child STDOUT & STDERR to variables

#print {*STDERR} 'in run_yum_query(), have $stdout_generated = ', "\n", '<<<=== BEGIN STDOUT ===>>>', $stdout_generated, '<<<=== END STDOUT ===>>>', "\n\n";
#print {*STDERR} 'in run_yum_query(), have $stderr_generated = ', "\n", '<<<=== BEGIN STDERR ===>>>', $stderr_generated, '<<<=== END STDERR ===>>>', "\n\n";

    my integer $test_exit_status = $CHILD_ERROR >> 8;

    if ( $test_exit_status == 0 ) {    # UNIX process return code 0, success
        # DEV NOTE: must print to STDERR in order to avoid delayed output
        print {*STDERR} "\n", ('[[[[[=====  YUM QUERY SUCCESS!  ', $module_or_distribution, '  =====]]]]]' . "\n") x 3, "\n";
    }
    elsif ($stderr_generated ne q{}) {
        die "\n", ('[[[[[==== YUM QUERY ERROR!  ', $module_or_distribution, '  =====]]]]]' . "\n") x 3, $stderr_generated, "\n", 'dying', "\n";
    }
    else {    # UNIX process return code not 0, error
        die "\n", ('[[[[[==== YUM QUERY ERROR!  ', $module_or_distribution, '  =====]]]]]' . "\n") x 3, 'dying', "\n";
    }

    my string_arrayref $repos = [];

    foreach my string $stdout_line (split /\n/, $stdout_generated) {
#print {*STDERR} 'in run_yum_query(), have $stdout_line = ' . $stdout_line . "\n";
       if ($stdout_line =~ m/Repo\s+:\s+([@]?\w+)/gxms) {
#print {*STDERR} 'in run_yum_query(), MATCH, have $1 = ' . q{'} . $1 . q{'} . "\n";
            push @{$repos}, $1;
       }
    }

    return $repos;
}

# RUN FPM COMMAND
# RUN FPM COMMAND
# RUN FPM COMMAND
sub run_fpm {
    { my string $RETURN_TYPE };
    ( my string $input_module, my string $output_format, my string $version_template, my string $work_dir, my string_arrayref $OS_SPECIFIC_DEPENDENCIES_DISTRIBUTION ) = @ARG;
print {*STDERR} 'in run_fpm(), received $input_module = ', q{'}, $input_module, q{'}, "\n";
print {*STDERR} 'in run_fpm(), received $output_format = ', q{'}, $output_format, q{'}, "\n";
print {*STDERR} 'in run_fpm(), received $version_template = ', q{'}, $version_template, q{'}, "\n";
print {*STDERR} 'in run_fpm(), received $work_dir = ', q{'}, $work_dir, q{'}, "\n";
print {*STDERR} 'in run_fpm(), received $OS_SPECIFIC_DEPENDENCIES_DISTRIBUTION = ', Dumper($OS_SPECIFIC_DEPENDENCIES_DISTRIBUTION), "\n";

    my string $stdout_file = $work_dir . '/' . $input_module . '.stdout';
    my string $stdout_generated = q{};
    my string $stderr_generated = q{};

    # create output-format-specific build options, right now this is limited to enabling source packages
    my string $output_format_options = q{};
    if ($output_format eq 'deb') {
        $output_format_options = ' --deb-NEED-ENABLE-SOURCE-PACKAGES';  # leading space required
    }
    elsif ($output_format eq 'rpm') {
        $output_format_options = ' --rpm-ba';  # leading space required
    }
    else {
        die 'CPANtoFPM ERROR, Invalid or unrecognized output format: ', q{'}, $output_format, q{'}, "\n", q{Only 'deb' and 'rpm' are currently supported, dying};
    }
    
    # NEED UPGRADE: remove hard-coded verbose option
    my string $output_verbose_options = q{};
    $output_verbose_options = ' --cpan-verbose';  # leading space required; DISABLE VIA COMMENT FOR NON-VERBOSE OUTPUT

    # DEB, START HERE: add deb version option here if needed, is there an "Epoch" equivalent for deb packages?
    # DEB, START HERE: add deb version option here if needed, is there an "Epoch" equivalent for deb packages?
    # DEB, START HERE: add deb version option here if needed, is there an "Epoch" equivalent for deb packages?
    
    # NEED UPGRADE: remove hard-coded version option???
    # DEV NOTE: must have Epoch > 4 due to Time::HiRes
    my string $version_options = q{};
    $version_options = q{ --rpm-tag 'Epoch: 10'};
    if ((defined $version_template) and ($version_template ne q{})) {
       $version_options .= ' -v ' . $version_template;
    }

    # if present, add OS-specific dependencies to spec file & rebuild RPM package
    my string $os_specific_dependencies_options = q{};
    if (scalar @{$OS_SPECIFIC_DEPENDENCIES_DISTRIBUTION}) {
print {*STDERR} 'in run_fpm(), OS-SPECIFIC DEPS, have $OS_SPECIFIC_DEPENDENCIES_DISTRIBUTION = ', Dumper($OS_SPECIFIC_DEPENDENCIES_DISTRIBUTION);
        
        # DEB, START HERE: add deb build options here, --deb-pre-depends and/or --deb-field
        # DEB, START HERE: add deb build options here, --deb-pre-depends and/or --deb-field
        # DEB, START HERE: add deb build options here, --deb-pre-depends and/or --deb-field

        # prepare additional OS-specific deps
        # HARD-CODED EXAMPLE:
        # Requires: FOO
        foreach my string $OS_SPECIFIC_DEPENDENCY (@{$OS_SPECIFIC_DEPENDENCIES_DISTRIBUTION}) {
            $os_specific_dependencies_options .= q{ --rpm-tag 'Requires: } . $OS_SPECIFIC_DEPENDENCY . q{'};
        }

print {*STDERR} 'in run_fpm(), OS-SPECIFIC DEPS, have $os_specific_dependencies_options = ', "\n", $os_specific_dependencies_options, "\n";

        # NEED UPGRADE: execute rpm deps-checking command on new rpm package
        # HARD-CODED EXAMPLE:
        # rpm -qpR /home/USERNAME/cpantofpm_tmp/package-rpm-build-SERIALNUMBER/RPMS/x86_64/perl-Encode-2.98-1.x86_64.rpm
#        ...

        # NEED UPGRADE: confirm OS-specific deps have been added to new rpm package
        # HARD-CODED EXAMPLE:
#        ...
    }

    # NEED UPGRADE: remove hard-coded display syntax
    my string $output_fpm_display = ' | tee ';    # display, yes simultaneous view & capture w/ tee
#    my string $output_fpm_display = ' > ';  # do NOT display, capture only

    # NEED UPGRADE: handle installation of a specific prior version of a module
    # HARD-CODED EXAMPLE:
    # fpm --verbose --debug-workspace --workdir /home/USERNAME/rpmbuild.fpm/ -t rpm -s cpan ExtUtils::MakeMaker
    # NEED RE-ENABLE TESTS???  --cpan-test
    # DEV NOTE: must include '--verbose' in all calls to provide $stdout_generated needed for parsing later
    my string_arrayref $fpm_command = [
        'bash',
        '-c',
        'set -o pipefail; unbuffer fpm --no-cpan-test --verbose' . $output_verbose_options .
        ' --debug-workspace --maintainer ' . $maintainer . ' --workdir ' . $work_dir . ' -t ' . $output_format .
        $output_format_options . $version_options . $os_specific_dependencies_options . ' -s cpan ' .
        $input_module . $output_fpm_display . $stdout_file
    ];  # ; color w/ unbuffer

print {*STDERR} 'in run_fpm(), have $fpm_command = ', Dumper($fpm_command), "\n";
print {*STDERR} 'in run_fpm(), about to call run3()...' . "\n";

#    run3( $execute_command, \undef, \$stdout_generated, \$stderr_generated );  # no simultaneous view & capture; child STDIN from /dev/null, child STDOUT & STDERR to variables
    run3( $fpm_command, undef, undef, \$stderr_generated );  # yes simultaneous view & capture w/ tee; child STDIN from parent STDIN, child STDOUT to parent STDOUT, child STDERR to variable
#    run3( $execute_command, \undef, \&stdout_print, \&stderr_print );  # no simultaneous view & capture; child STDOUT & STDERR to subroutines

    #  yes simultaneous view & capture w/ tee; read STDOUT contents from file saved by `tee` command
    open(my filehandleref $STDOUT_FILE_FH, '<', $stdout_file)
        or die 'CPANtoFPM ERROR, Can not open STDOUT file: ', q{'}, $stdout_file, q{'}, "\n", $OS_ERROR, "\n", 'dying';
    while (my string $stdout_file_line = <$STDOUT_FILE_FH>) {
        $stdout_generated .= $stdout_file_line;
    }
    close $STDOUT_FILE_FH
        or die 'CPANtoFPM ERROR, Can not close STDOUT file: ', q{'}, $stdout_file, q{'}, "\n", $OS_ERROR, "\n", 'dying';    

###print {*STDERR} 'in run_fpm(), have $stdout_generated = ', "\n", '<<<=== BEGIN STDOUT ===>>>', $stdout_generated, '<<<=== END STDOUT ===>>>', "\n\n";
###print {*STDERR} 'in run_fpm(), have $stderr_generated = ', "\n", '<<<=== BEGIN STDERR ===>>>', $stderr_generated, '<<<=== END STDERR ===>>>', "\n\n";

    my integer $test_exit_status = $CHILD_ERROR >> 8;

    if ( $test_exit_status == 0 ) {    # UNIX process return code 0, success
        # DEV NOTE: must print to STDERR in order to avoid delayed output
        print {*STDERR} "\n", ('[[[[[=====  FPM SUCCESS!  ', $input_module, '  =====]]]]]' . "\n") x 3, "\n";
    }
    else {    # UNIX process return code not 0, error
        die "\n", ('[[[[[==== FPM ERROR!  ', $input_module, '  =====]]]]]' . "\n") x 3, 'dying', "\n";
    }

    return $stdout_generated;
}

# START HERE: remove $version_template from below and elsewhere???
# START HERE: remove $version_template from below and elsewhere???
# START HERE: remove $version_template from below and elsewhere???

# DEB, DETERMINE CONTROL FILE NAME
# DEB, DETERMINE CONTROL FILE NAME
# DEB, DETERMINE CONTROL FILE NAME
sub determine_control_file {
    { my string $RETURN_TYPE };
    ( my string $stdout_generated ) = @ARG;

    my @stdout_generated_lines_array = split( "\n", $stdout_generated );
    my string_arrayref $stdout_generated_lines = \@stdout_generated_lines_array;

    # HARD-CODED EXAMPLE: 
    # Creating {:path=>"/home/USERNAME/cpantofpm_tmp/package-deb-build-SERIALNUMBER/control.tar.gz",
    #    :from=>"/home/USERNAME/cpantofpm_tmp/package-deb-build-SERIALNUMBER/control", :level=>:info}
    my string $control_file = q{};
    foreach my string $stdout_generated_line (@{$stdout_generated_lines}) {
        if ($stdout_generated_line =~ m/^.*\"($work_dir.*\/control)\",.*$/) {
            $control_file = $1;
            last;
        }
    }

    if ($control_file eq q{}) {
        die 'CPANtoFPM ERROR, Can not determine DEB control file, dying';
    }

###print {*STDERR} 'in determine_control_file(), have $control_file = ', q{'}, $control_file, q{'}, "\n";

    return $control_file;
}

# DEB, UNZIP/OPEN CONTROL FILE & DETERMINE DEPENDENCIES
# DEB, UNZIP/OPEN CONTROL FILE & DETERMINE DEPENDENCIES
# DEB, UNZIP/OPEN CONTROL FILE & DETERMINE DEPENDENCIES
sub determine_control_file_dependencies {
    { my string_hashref_hashref $RETURN_TYPE };
    ( my string $control_file ) = @ARG;

    # tar -xzvf /home/USERNAME/cpantofpm_tmp/package-deb-build-SERIALNUMBER/control.tar.gz
    my string $control_dir = substr $control_file, 0, -7;
    my string $control_tarball = $control_file . '.tar.gz';
    my string $tar_command = 'cd ' . $control_dir . ' && tar -xzvf ' . $control_tarball;
    my string $tar_output = `$tar_command`;

###print {*STDERR} 'in determine_control_file_dependencies(), have $tar_command = ', "\n", $tar_command, "\n";
###print {*STDERR} 'in determine_control_file_dependencies(), have $tar_output = ', "\n", $tar_output, "\n";

    open(my filehandleref $CONTROL_FILE_FH, '<', $control_file) 
        or die 'CPANtoFPM ERROR, Can not open DEB control file: ', q{'}, $control_file, q{'}, "\n", $OS_ERROR, "\n", 'dying';
    my string_hashref_hashref $dependencies = {};
    while (my string $control_file_line = <$CONTROL_FILE_FH>) {
        chomp $control_file_line;

        # HARD-CODED EXAMPLE:
        # Depends: perl(Exporter) >= 5.57, perl(Storable), perl(parent) >= 0.221
        # perl (>= 5.006)  CURRENTLY IGNORED
        if ($control_file_line =~ m/^Depends:\ /) {
            my string $control_file_depends_line = substr $control_file_line, 9;  # discard leading 'Depends: '
            my @control_file_depends = split /, /, $control_file_depends_line;
            foreach my string $control_file_depend (@control_file_depends) {
                # HARD-CODED EXAMPLES:
                # perl(Encode)
                # perl(Data::Dumper)
                if ($control_file_depend =~ m/^perl\((.*)\)$/) {
                    $dependencies->{$1} = {};
                }
                # NEED ANSWER: is single equal sign correct here?
                # HARD-CODED EXAMPLES:
                # perl(Foo) =  2.3.1
                # perl(Bar) = v4.2.0
                elsif ($control_file_depend =~ m/^perl\((.*)\)\s+=\s+([v.\d]+)$/) {
                    $dependencies->{$1} = { _version => $2 };
                }
                # HARD-CODED EXAMPLES:
                # perl(File::Spec) >= 0.8
                # perl(MongoDB) >= v1.8.0
                elsif ($control_file_depend =~ m/^perl\((.*)\)\s+>=\s+([v.\d]+)$/) {
                    $dependencies->{$1} = { _min_version => $2 };
                }

# DEB, START HERE: need hard-coded example of non-Perl dependency, re-enable below code & test
# DEB, START HERE: need hard-coded example of non-Perl dependency, re-enable below code & test
# DEB, START HERE: need hard-coded example of non-Perl dependency, re-enable below code & test

                # allow non-Perl dependencies, do not die
#                else {
#                    die 'CPANtoFPM ERROR, DEB control file contains unrecognized Perl dependency: ', q{'}, $control_file_depend, q{'}, "\n", 'dying';
#                }
            }
        }
    }
    close $CONTROL_FILE_FH
        or die 'CPANtoFPM ERROR, Can not close DEB control file: ', q{'}, $control_file, q{'}, "\n", $OS_ERROR, "\n", 'dying';

###print {*STDERR} 'in determine_control_file_dependencies(), have $dependencies = ', Dumper($dependencies), "\n";

    return $dependencies;
}

# RPM, SAVE COPIES OF THE RPM & SRPM & SPEC FILES
# RPM, SAVE COPIES OF THE RPM & SRPM & SPEC FILES
# RPM, SAVE COPIES OF THE RPM & SRPM & SPEC FILES
sub save_rpm_srpm_spec {
    { my string_hashref $RETURN_TYPE };
    ( my string $stdout_generated ) = @ARG;

    my @stdout_generated_lines_array = split( "\n", $stdout_generated );
    my string_arrayref $stdout_generated_lines = \@stdout_generated_lines_array;

    # HARD-CODED EXAMPLE:
    # Running rpmbuild {:args=>["rpmbuild", "--ba", 
    #     "--define", "buildroot /home/USERNAME/cpantofpm_tmp/package-rpm-build-SERIALNUMBER/BUILD", 
    #     "--define", "_topdir /home/USERNAME/cpantofpm_tmp/package-rpm-build-SERIALNUMBER", 
    #     "--define", "_sourcedir /home/USERNAME/cpantofpm_tmp/package-rpm-build-SERIALNUMBER", 
    #     "--define", "_rpmdir /home/USERNAME/cpantofpm_tmp/package-rpm-build-SERIALNUMBER/RPMS", 
    #     "--define", "_tmppath /home/USERNAME/cpantofpm_tmp", 
    #     "/home/USERNAME/cpantofpm_tmp/package-rpm-build-SERIALNUMBER/SPECS/perl-Data-Dumper.spec"], :level=>:info}
    # ...
    # Wrote: /home/USERNAME/cpantofpm_tmp/package-rpm-build-SERIALNUMBER/SRPMS/perl-Data-Dumper-2.161-1.src.rpm {:level=>:info}
    # Wrote: /home/USERNAME/cpantofpm_tmp/package-rpm-build-SERIALNUMBER/RPMS/x86_64/perl-Data-Dumper-2.161-1.x86_64.rpm {:level=>:info}
    # ...
    # Created package {:path=>"perl-Data-Dumper-2.161-1.x86_64.rpm"}
    my string $rpm_file = q{};
    my string $srpm_file = q{};
    my string $srpm_file_tmp = q{};
    my string $spec_file = q{};
    my string $spec_file_tmp = q{};
    foreach my string $stdout_generated_line (@{$stdout_generated_lines}) {
####print {*STDERR} 'in save_rpm_srpm_spec(), have $stdout_generated_line = ', "\n", $stdout_generated_line, "\n";

        if ($stdout_generated_line =~ m/^Created\ package\ \{:path=>\"(.*)\"\}$/) {
            if ($rpm_file ne q{}) {
                die 'CPANtoFPM ERROR, Already found RPM package file name once: ', q{'}, $rpm_file, q{'}, "\n",
                    'Found RPM package file name a second time: ', q{'}, $1, q{'}, 'dying';
            }
            $rpm_file = $1;
        }
        elsif ($stdout_generated_line =~ m/^.*Wrote:\ (.*\.src\.rpm)\ .*$/) {  # leading .* is to allow color code escape characters, I think!
            if ($srpm_file_tmp ne q{}) {
                die 'CPANtoFPM ERROR, Already found SRPM source package file name once: ', q{'}, $srpm_file_tmp, q{'}, "\n",
                    'Found SRPM source package file name a second time: ', q{'}, $1, q{'}, 'dying';
            }
            $srpm_file_tmp = $1;
        }
        elsif ($stdout_generated_line =~ m/^.*\"($work_dir.*\.spec)\"],.*$/) {
            if ($spec_file_tmp ne q{}) {
                die 'CPANtoFPM ERROR, Already found RPM spec file name once: ', q{'}, $spec_file_tmp, q{'}, "\n",
                    'Found RPM spec file name a second time: ', q{'}, $1, q{'}, 'dying';
            }
            $spec_file_tmp = $1;
        }
    }

    if ($rpm_file eq q{}) {
        die 'CPANtoFPM ERROR, Can not determine RPM package file, dying';
    }
    elsif ($srpm_file_tmp eq q{}) {
        die 'CPANtoFPM ERROR, Can not determine SRPM source package file, dying';
    }
    elsif ($spec_file_tmp eq q{}) {
        die 'CPANtoFPM ERROR, Can not determine RPM spec file, dying';
    }

###print {*STDERR} 'in save_rpm_srpm_spec(), have $rpm_file = ', q{'}, $rpm_file, q{'}, "\n";
###print {*STDERR} 'in save_rpm_srpm_spec(), have $srpm_file_tmp = ', q{'}, $srpm_file_tmp, q{'}, "\n";
###print {*STDERR} 'in save_rpm_srpm_spec(), have $spec_file_tmp = ', q{'}, $spec_file_tmp, q{'}, "\n";

    # determine package arch from RPM file name, if present
    # HARD-CODED EXAMPLES:
    # perl-Data-Dumper-2.161-1.x86_64.rpm
    # perl-ExtUtils-MakeMaker-7.34-1.noarch.rpm
    my @rpm_file_split = split /\./, $rpm_file;
    pop @rpm_file_split;  # discard 'rpm' file suffix
    my string $arch_package = pop @rpm_file_split;  # capture 'noarch' or 'x86_64' or 'i386' etc.

###print {*STDERR} 'in save_rpm_srpm_spec(), have $arch_package = ', q{'}, $arch_package, q{'}, "\n";

    # save cleaned $srpm_file and $spec_file, not instance-specific $srpm_file_tpm and $spec_file_tmp full file paths
    (undef, undef, $srpm_file) = File::Spec->splitpath( $srpm_file_tmp , 0);  # second argument false in *nix to return actual file, $no_file = 0
    (undef, undef, $spec_file) = File::Spec->splitpath( $spec_file_tmp , 0);  # second argument false in *nix to return actual file, $no_file = 0
    my string_hashref $package_info = {rpm_file => $rpm_file, srpm_file => $srpm_file,  spec_file => $spec_file, arch => $arch_package };

    # create dirs & move/copy files
    my string_hashref $package_dirs_relative_and_files = { RPMS => $rpm_file, SRPMS => $srpm_file_tmp, SPECS => $spec_file_tmp };
    foreach my string $package_dir_relative (keys %{$package_dirs_relative_and_files}) {
        my string $package_file = $package_dirs_relative_and_files->{$package_dir_relative};

        # ensure dirs exist in current working dir
        my string $current_dir = File::Spec->curdir();
        my string $package_dir_path = File::Spec->catpath( q{}, $current_dir, $package_dir_relative );  # first argument is ignored in *nix, $volume = q{}
###print {*STDERR} 'in save_rpm_srpm_spec(), have $package_dir_path = ', q{'}, $package_dir_path, q{'}, "\n";
        $package_info->{$package_dir_relative . '_dir'} = $package_dir_path;
        if ((-e $package_dir_path) and (not -d $package_dir_path)) {
            die 'CPANtoFPM ERROR, Pre-existing non-directory in current working directory: ', $package_dir_path, "\n", 'dying';
        }
        if (not -e $package_dir_path) {
            make_path $package_dir_path
                or die 'CPANtoFPM ERROR, Failed to create directory in current working directory: ', $package_dir_path, "\n", $OS_ERROR, "\n", 'dying';
            print 'Directory successfully created in current working directory: ', $package_dir_path, "\n";
        }

        # move or copy package files to package dirs in current working dir; RPMS dir will later be renamed when $arch_master is determined
        (my string $package_file_volume, my string $package_file_dirs, my string $package_file_relative) = File::Spec->splitpath( $package_file , 0);  # second argument false in *nix to return actual file, $no_file = 0
###print {*STDERR} 'in save_rpm_srpm_spec(), have $package_file_volume = ', q{'}, $package_file_volume, q{'}, "\n";
###print {*STDERR} 'in save_rpm_srpm_spec(), have $package_file_dirs = ', q{'}, $package_file_dirs, q{'}, "\n";
###print {*STDERR} 'in save_rpm_srpm_spec(), have $package_file_relative = ', q{'}, $package_file_relative, q{'}, "\n";
        my string $package_file_new = File::Spec->catpath( q{}, $package_dir_path, $package_file_relative );  # first argument is ignored in *nix, $volume = q{}
###print {*STDERR} 'in save_rpm_srpm_spec(), have $package_file_new = ', q{'}, $package_file_new, q{'}, "\n";

        # move RPM files because already copied by fpm, copy SRPM & SPEC files because ignored by fpm
        if ($package_dir_relative eq 'RPMS') {
            move($package_file, $package_file_new)
                or die 'CPANtoFPM ERROR, Failed to move package file to package directory in current working directory, move from: ', q{'}, $package_file, q{'}, "\n",
                    'Move to: ', q{'}, $package_file_new, q{'}, "\n", $OS_ERROR, "\n", 'dying';
        }
        else {
            copy($package_file, $package_file_new)
                or die 'CPANtoFPM ERROR, Failed to copy package file to package directory in current working directory, copy from: ', q{'}, $package_file, q{'}, "\n",
                    'Copy to: ', q{'}, $package_file, q{'}, "\n", $OS_ERROR, "\n", 'dying';
        }
     }

    return $package_info;
}

# RPM, OPEN SPEC FILE & DETERMINE DEPENDENCIES
# RPM, OPEN SPEC FILE & DETERMINE DEPENDENCIES
# RPM, OPEN SPEC FILE & DETERMINE DEPENDENCIES
sub determine_spec_file_dependencies {
    { my string_hashref_hashref $RETURN_TYPE };
    ( my string_hashref $package_info ) = @ARG;

    my string $spec_file_path = File::Spec->catpath( q{}, $package_info->{SPECS_dir}, $package_info->{spec_file} );  # first argument is ignored in *nix, $volume = q{}

    open(my filehandleref $SPEC_FILE_FH, '<', $spec_file_path) 
        or die 'CPANtoFPM ERROR, Can not open RPM spec file: ', q{'}, $spec_file_path, q{'}, "\n", $OS_ERROR, "\n", 'dying';
    my string_hashref_hashref $dependencies = {};
    while (my string $spec_file_line = <$SPEC_FILE_FH>) {
        chomp $spec_file_line;
        if ($spec_file_line =~ m/^Requires:\ perl\(/) {
            # HARD-CODED EXAMPLES:
            # Requires: perl(Encode)
            # Requires: perl(Data::Dumper)
            if ($spec_file_line =~ m/^Requires:\ perl\((.*)\)$/) {
                $dependencies->{$1} = {};
            }
            # NEED ANSWER: is single equal sign correct here?
            # HARD-CODED EXAMPLES:
            # Requires: perl(Foo) =  2.3.1
            # Requires: perl(Bar) = v4.2.0
            # Requires: perl(Baz) = 100_456.600_001
            # Requires: perl(Bax) = v20_23.21_12
            elsif ($spec_file_line =~ m/^Requires:\ perl\((.*)\)\s+=\s+([v._\d]+)$/) {
                $dependencies->{$1} = { _version => $2 };
            }
            # HARD-CODED EXAMPLES:
            # Requires: perl(File::Spec) >= 0.8
            # Requires: perl(MongoDB) >= v1.8.0
            # Requires: perl(Test::More) >= 0.60_01
            # Requires: perl(Test::More) >= v90_0.60_01
            elsif ($spec_file_line =~ m/^Requires:\ perl\((.*)\)\s+>=\s+([v._\d]+)$/) {
                $dependencies->{$1} = { _min_version => $2 };
            }
            else {
                die 'CPANtoFPM ERROR, RPM spec file contains unrecognized Perl dependency line: ', q{'}, $spec_file_line, q{'}, "\n", 'dying';
            }
        }
    }
    close $SPEC_FILE_FH
        or die 'CPANtoFPM ERROR, Can not close RPM spec file: ', q{'}, $spec_file_path, q{'}, "\n", $OS_ERROR, "\n", 'dying';

###print {*STDERR} 'in determine_spec_file_dependencies(), have $dependencies = ', Dumper($dependencies), "\n";

    $dependencies->{_package_info} = $package_info;
    return $dependencies;
}

# RPM, RENAME RPMS DIRECTORY
# RPM, RENAME RPMS DIRECTORY
# RPM, RENAME RPMS DIRECTORY
sub rename_rpms_dir {
    { my void $RETURN_TYPE };
    ( my string $RPMS_dir, my string $arch_master ) = @ARG;

print {*STDERR} 'in rename_rpms_dir(), received $RPMS_dir = ', q{'}, $RPMS_dir, q{'}, "\n";
print {*STDERR} 'in rename_rpms_dir(), received $arch_master = ', q{'}, $arch_master, q{'}, "\n";

    # rename RPMS/ directory to $arch_master
    if ((not defined $arch_master) or ($arch_master eq q{})) {
        die 'CPANtoFPM ERROR, Failed to rename RPMS/ directory to be architecture-specific, no architecture determined, not even ', q{'}, 'noarch', q{'}, ', dying';
    }
    elsif (-e $arch_master) {
        die 'CPANtoFPM ERROR, Failed to rename RPMS/ directory to be architecture-specific, destination already exists in current working directory: ', q{'}, $arch_master, q{'}, ', dying';
    }
    move($RPMS_dir, $arch_master)
        or die 'CPANtoFPM ERROR, Failed to rename RPMS/ directory to be architecture-specific, move from: ', q{'}, $RPMS_dir, q{'}, "\n",
            'Move to: ', q{'}, $arch_master, q{'}, "\n", $OS_ERROR, "\n", 'dying';
}

# SAVE DEPENDENCIES FILE
# SAVE DEPENDENCIES FILE
# SAVE DEPENDENCIES FILE
sub save_deps_file {
    { my string_hashref_hashref $RETURN_TYPE };
    ( my string $module, my string $distribution, my string_hashref_hashref $dependencies, my string_hashref_hashref $dependencies_flat, my string_hashref_hashref $dependencies_full ) = @ARG;

print {*STDERR} 'in save_deps_file(), received $module = ', q{'}, $module, q{'}, "\n";
print {*STDERR} 'in save_deps_file(), received $distribution = ', q{'}, $distribution, q{'}, "\n";
print {*STDERR} 'in save_deps_file(), received $dependencies = ', Dumper($dependencies);
print {*STDERR} 'in save_deps_file(), received $dependencies_flat = ', Dumper($dependencies_flat);
print {*STDERR} 'in save_deps_file(), received $dependencies_full = ', Dumper($dependencies_full);

    # each module must also exist in flattened $dependencies_flat, because we're going to save that data in the deps file as well
    if (not exists $dependencies_flat->{$module}) {
        # this module has no dependencies!  create empty hashref to avoid errors when saving file
        $dependencies_flat->{$module} = {_distribution => $distribution};
print {*STDERR} 'in save_deps_file(), have updated-with-empty-dependency $dependencies_flat = ', Dumper($dependencies_flat);
    }

    # double-check nested vs flattened deps, prep
    my integer_hashref $dependencies_flat_simple = flatten_simplify_dependencies($module, $dependencies, $dependencies_full);
    if (exists $dependencies_flat_simple->{$module}) {
        delete $dependencies_flat_simple->{$module};
print {*STDERR} 'in save_deps_file(), SKIPPING MODULE DUE TO CIRCULAR DEPENDENCY, will not make module its own dependency: ', q{'}, $module, q{'}, "\n";
    }
print {*STDERR} 'in save_deps_file(), have $dependencies_flat_simple = ', Dumper($dependencies_flat_simple);
print {*STDERR} 'in save_deps_file(), have $dependencies_flat->{$module} = ', Dumper($dependencies_flat->{$module});




# START HERE: dep counts & content no longer matches
# START HERE: dep counts & content no longer matches
# START HERE: dep counts & content no longer matches


    # double-check nested vs flattened deps, counts
    my integer $dependencies_count = (scalar keys %{$dependencies_flat_simple});
    my integer $dependencies_flat_count = (scalar keys %{$dependencies_flat->{$module}}) - 1;  # subtract 1 for '_distribution' key

#=DISABLED_NEED_FIX
    if ($dependencies_count != $dependencies_flat_count) {
        # DEV NOTE, CORRELATION #ctf01: no-dep counts match correctly when manually setting $dependencies_flat data structure 
        # don't die if ($dependencies_count = 0) and ($dependencies_flat_count = -1), this just means there are no deps at all
#        if (($dependencies_count > 0) or ($dependencies_flat_count > 0)) {
            die 'CPANtoFPM ERROR, Failed to properly determine dependencies, count mistmatch, have $dependencies_count = ', $dependencies_count, ', have $dependencies_flat_count = ', $dependencies_flat_count, ', dying';
#        }
    }

    # double-check nested vs flattened deps, content
    foreach my string $dependencies_flat_key (keys %{$dependencies_flat->{$module}}) {
        next if ($dependencies_flat_key eq '_distribution');  # disregard key not present in $dependencies_flat_simple
        if ((not exists $dependencies_flat->{$module}->{$dependencies_flat_key}->{count}) or (not defined $dependencies_flat->{$module}->{$dependencies_flat_key}->{count})) {
            die 'CPANtoFPM ERROR, Failed to properly determine dependencies, have non-existent or undefined $dependencies_flat->{', $module, '}->{', $dependencies_flat_key, '}->{count}, dying';
        }
        if ((not exists $dependencies_flat->{$module}->{$dependencies_flat_key}->{distribution}) or (not defined $dependencies_flat->{$module}->{$dependencies_flat_key}->{distribution})) {
            die 'CPANtoFPM ERROR, Failed to properly determine dependencies, have non-existent or undefined $dependencies_flat->{', $module, '}->{', $dependencies_flat_key, '}->{distribution}, dying';
        }
        if ((not exists $dependencies_flat_simple->{$dependencies_flat_key}) or (not defined $dependencies_flat_simple->{$dependencies_flat_key})) {
            die 'CPANtoFPM ERROR, Failed to properly determine dependencies, have non-existent or undefined $dependencies_flat_simple->{', $dependencies_flat_key, '}, dying';
        }
        # NEED UPGRADE: data structures and counts do not match past this point, must upgrade data structures to match and properly generate counts
#        if ((not exists $dependencies_flat_simple->{$dependencies_flat_key}->{count}) or (not defined $dependencies_flat_simple->{$dependencies_flat_key}->{count})) {
#            die 'CPANtoFPM ERROR, Failed to properly determine dependencies, have non-existent or undefined $dependencies_flat_simple->{', $dependencies_flat_key, '}->{count}, dying';
#        }
#        if ((not exists $dependencies_flat_simple->{$dependencies_flat_key}->{distribution}) or (not defined $dependencies_flat_simple->{$dependencies_flat_key}->{distribution})) {
#            die 'CPANtoFPM ERROR, Failed to properly determine dependencies, have non-existent or undefined $dependencies_flat_simple->{', $dependencies_flat_key, '}->{distribution}, dying';
#        }
#        if ($dependencies_flat->{$module}->{$dependencies_flat_key}->{count} != $dependencies_flat_simple->{$dependencies_flat_key}->{count}) {
#            die 'CPANtoFPM ERROR, Failed to properly determine dependencies, content mistmatch, have $dependencies_flat->{', $module, '}->{', $dependencies_flat_key, '}->{count} = ', $dependencies_flat->{$dependencies_flat_key}->{count}, ', have $dependencies_flat_simple->{', $dependencies_flat_key, '}->{count} = ', $dependencies_flat_simple->{$dependencies_flat_key}->{count}, ', dying';
#        }
#        if ($dependencies_flat->{$module}->{$dependencies_flat_key}->{distribution} ne $dependencies_flat_simple->{$dependencies_flat_key}->{distribution}) {
#            die 'CPANtoFPM ERROR, Failed to properly determine dependencies, content mistmatch, have $dependencies_flat->{', $module, '}->{', $dependencies_flat_key, '}->{distribution} = ', $dependencies_flat->{$dependencies_flat_key}->{distribution}, ', have $dependencies_flat_simple->{', $dependencies_flat_key, '}->{distribution} = ', $dependencies_flat_simple->{$dependencies_flat_key}->{distribution}, ', dying';
#        }
    }
#=cut

    # re-skip skipped dependencies, deps file for this skipped distribution will be saved from another part of the dependency tree
    if (exists $dependencies->{_skipped}) {
print {*STDERR} 'in save_deps_file(), SKIPPING SKIPPED DEPENDENCY: ', q{'}, $module, q{'}, '  ', $dependencies->{_skipped}, "\n";
        return $dependencies;
    }


    # DEB, THEN START HERE: remove or supplement RPM-specific logic below
    # DEB, THEN START HERE: remove or supplement RPM-specific logic below
    # DEB, THEN START HERE: remove or supplement RPM-specific logic below


    # DEV NOTE: process dependencies by module, but save deps files by distribution, as is done with all other saved files

    # construct deps file name based on distribution, as contained in rpm file name
    my string $deps_file_relative = $dependencies->{_package_info}->{rpm_file};
    if ((substr $deps_file_relative, -4, 4) ne '.rpm') {
        die q{CPANtoFPM ERROR, package name does not end in '.rpm': '}, $deps_file_relative, q{'}, "\n", 'dying';
    }
    substr $deps_file_relative, -4, 4, '.deps';  # replace '.rpm' w/ '.deps'

    # save deps file as part of package info
    $dependencies->{_package_info}->{deps_file} = $deps_file_relative;
print {*STDERR} 'in save_deps_file(), have $deps_file_relative = ', q{'}, $deps_file_relative, q{'}, "\n";

    # ensure DEPS dir exists in current working dir
    my string $current_dir = File::Spec->curdir();
    my string $deps_dir_path = File::Spec->catpath( q{}, $current_dir, 'DEPS' );  # first argument is ignored in *nix, $volume = q{}
print {*STDERR} 'in save_deps_file(), have $deps_dir_path = ', q{'}, $deps_dir_path, q{'}, "\n";
    if ((-e $deps_dir_path) and (not -d $deps_dir_path)) {
        die 'CPANtoFPM ERROR, Pre-existing non-directory in current working directory: ', $deps_dir_path, "\n", 'dying';
    }
    if (not -e $deps_dir_path) {
        make_path $deps_dir_path
            or die 'CPANtoFPM ERROR, Failed to create directory in current working directory: ', $deps_dir_path, "\n", $OS_ERROR, "\n", 'dying';
        print 'DEPS directory successfully created in current working directory: ', $deps_dir_path, "\n";
    }

    # splitpath() DEPS dir in order to keep current working dir and 'DEPS' together
    (my string $deps_dir_volume, my string $deps_dir_dirs, undef) = File::Spec->splitpath( $deps_dir_path , 1);  # third retval undef, no file only dir; second argument false in *nix to return directory, $no_file = 1
###print {*STDERR} 'in save_deps_file(), have $deps_dir_volume = ', q{'}, $deps_dir_volume, q{'}, "\n";
print {*STDERR} 'in save_deps_file(), have $deps_dir_dirs = ', q{'}, $deps_dir_dirs, q{'}, "\n";
    my string $deps_file = File::Spec->catpath( q{}, $deps_dir_path, $deps_file_relative );  # first argument is ignored in *nix, $volume = q{}
print {*STDERR} 'in save_deps_file(), have $deps_file = ', q{'}, $deps_file, q{'}, "\n";

    # actually save deps file
    open(my filehandleref $DEPENDENCIES_FH, '>', $deps_file)
        or die 'CPANtoFPM ERROR, Can not open dependencies file for writing: ', $deps_file, "\n", 'dying';
    print $DEPENDENCIES_FH 'my string $distribution = ', q{'}, $distribution, q{'}, ';', "\n";
    print $DEPENDENCIES_FH 'my integer $dependencies_flat_count = ', $dependencies_flat_count, ';', "\n";
    print $DEPENDENCIES_FH 'my integer $dependencies_count = ', $dependencies_count, ';', "\n";
    print $DEPENDENCIES_FH 'my integer_hashref $dependencies_flat_simple = ', Dumper($dependencies_flat_simple);
    print $DEPENDENCIES_FH 'my string_hashref_hashref $dependencies_flat = ', Dumper($dependencies_flat->{$module});
    print $DEPENDENCIES_FH 'my string_hashref_hashref $dependencies = ', Dumper($dependencies);
    close $DEPENDENCIES_FH
        or die 'CPANtoFPM ERROR, Can not close dependencies file after writing: ', $deps_file, "\n", 'dying';

    return $dependencies;
}

# FLATTEN & SIMPLIFY NESTED DEPENDENCIES, FOR CHECKING ALREADY-FLATTENED DEPENDENCIES
# FLATTEN & SIMPLIFY NESTED DEPENDENCIES, FOR CHECKING ALREADY-FLATTENED DEPENDENCIES
# FLATTEN & SIMPLIFY NESTED DEPENDENCIES, FOR CHECKING ALREADY-FLATTENED DEPENDENCIES
sub flatten_simplify_dependencies {
    { my integer_hashref $RETURN_TYPE };
    ( my string $module, my string_hashref_hashref $dependencies, my string_hashref_hashref $dependencies_full ) = @ARG;
print {*STDERR} 'in flatten_simplify_dependencies(), received $module = ', q{'}, $module, q{'}, "\n";
print {*STDERR} 'in flatten_simplify_dependencies(), received $dependencies = ', Dumper($dependencies);
print {*STDERR} 'in flatten_simplify_dependencies(), received $dependencies_full = ', Dumper($dependencies_full);

# NEED UPGRADE: everywhere `return { $module => 1 };` appears below, the count MAY need to be increased, debug and figure out how to properly calculate counts for double-checking against deps flattened during runtime in save_deps_file(), also make sure already-flattened dep counts are correct in the first place
# NEED UPGRADE: everywhere `return { $module => 1 };` appears below, the count MAY need to be increased, debug and figure out how to properly calculate counts for double-checking against deps flattened during runtime in save_deps_file(), also make sure already-flattened dep counts are correct in the first place
# NEED UPGRADE: everywhere `return { $module => 1 };` appears below, the count MAY need to be increased, debug and figure out how to properly calculate counts for double-checking against deps flattened during runtime in save_deps_file(), also make sure already-flattened dep counts are correct in the first place

    if (exists $dependencies->{_skipped}) {
        if (not defined $dependencies->{_skipped}) {
            die 'CPANtoFPM ERROR, Failed to properly define skipped dependencies tag: ', q{'}, $dependencies->{_distribution}, q{'}, "\n", 'dying';
        }
        elsif ($dependencies->{_skipped} eq 'perl_core_cpan_shell') {
print {*STDERR} 'in flatten_simplify_dependencies(), found SKIPPED perl_core_cpan_shell, taking no action because none is necessary...', "\n";
            return {};
        }
        elsif ($dependencies->{_skipped} eq 'perl_core_module_corelist') {
print {*STDERR} 'in flatten_simplify_dependencies(), found SKIPPED perl_core_module_corelist, taking no action because none is necessary...', "\n";
            return {};
        }
        elsif ($dependencies->{_skipped} eq 'on_stack_module') {
print {*STDERR} 'in flatten_simplify_dependencies(), found SKIPPED on_stack_module CIRCULAR DEPENDENCY, taking no action, non-skipped instance will be processed later...', "\n";
#            return { $module => 1 };
        }
        elsif ($dependencies->{_skipped} eq 'already_processed_module') {
print {*STDERR} 'in flatten_simplify_dependencies(), found SKIPPED already_processed_module, NEED ADD ACTION HERE...', "\n";
#            return { $module => 1 };
        }
        elsif ($dependencies->{_skipped} eq 'on_stack_distribution') {
print {*STDERR} 'in flatten_simplify_dependencies(), found SKIPPED on_stack_distribution CIRCULAR DEPENDENCY, taking no action, non-skipped instance will be processed later...', "\n";
#            return { $module => 1 };
        }
        elsif ($dependencies->{_skipped} eq 'already_processed_distribution') {
print {*STDERR} 'in flatten_simplify_dependencies(), found SKIPPED already_processed_distribution, NEED ADD ACTION HERE...', "\n";
#            return { $module => 1 };
        }
        elsif ($dependencies->{_skipped} eq 'on_stack_sibling') {
print {*STDERR} 'in flatten_simplify_dependencies(), found SKIPPED on_stack_sibling CIRCULAR DEPENDENCY, taking no action, non-skipped instance will be processed later...', "\n";
#            return { $module => 1 };
        }
        elsif ($dependencies->{_skipped} eq 'already_processed_sibling') {
print {*STDERR} 'in flatten_simplify_dependencies(), found SKIPPED already_processed_sibling, NEED ADD ACTION HERE...', "\n";
#            return { $module => 1 };
        }
        elsif ($dependencies->{_skipped} eq 'already_processed_distribution_prior_execution') {
print {*STDERR} 'in flatten_simplify_dependencies(), found SKIPPED already_processed_distribution_prior_execution, NEED ADD ACTION HERE...', "\n";

# THEN START HERE: don't die, need load deps file & recursively call flatten_simplify_dependencies()
# THEN START HERE: don't die, need load deps file & recursively call flatten_simplify_dependencies()
# THEN START HERE: don't die, need load deps file & recursively call flatten_simplify_dependencies()

            die 'CPANtoFPM ERROR, Can not load deps file, not yet implemented, dying';
        }
    }

    my integer_hashref $dependencies_flat_simple = {};
    foreach my string_hashref $dependency (sort keys %{$dependencies}) {
        next if ((substr $dependency, 0, 1) eq '_');
print {*STDERR} 'in flatten_simplify_dependencies(), top of foreach() loop, have $dependency = ', q{'}, $dependency, q{'}, "\n";
        # create this dep's flattened key
        if (not exists $dependencies_flat_simple->{$dependency}) {
            $dependencies_flat_simple->{$dependency} = 0;
        }
        $dependencies_flat_simple->{$dependency} += 1;
        
        # recurse
print {*STDERR} 'in flatten_simplify_dependencies(), about to call flatten_simplify_dependencies()...', "\n";
        my integer_hashref $dependencies_flat_simple_recursed = flatten_simplify_dependencies($dependency, $dependencies->{$dependency}, $dependencies_full);
print {*STDERR} 'in flatten_simplify_dependencies(), ret from call to flatten_simplify_dependencies(), have $dependencies_flat_simple_recursed = ', Dumper($dependencies_flat_simple_recursed);

        # merge in recursed flattened deps
#        $dependencies_flat_simple = {%{$dependencies_flat_simple}, %{$dependencies_flat_simple_recursed}};  # bad, overwrite counts instead of accumulating
        foreach my string $dependency_recursed (keys %{$dependencies_flat_simple_recursed}) {
            if (not exists $dependencies_flat_simple->{$dependency_recursed}) {
                $dependencies_flat_simple->{$dependency_recursed} = 0;
            }
            $dependencies_flat_simple->{$dependency_recursed} += $dependencies_flat_simple_recursed->{$dependency_recursed};
        }
    }
    return $dependencies_flat_simple;
}

# does not allow simultaneous viewing & capturing
#sub stdout_print { { my void $RETURN_TYPE }; ( my string $input_string ) = @ARG; print $input_string; }
#sub stderr_print { { my void $RETURN_TYPE }; ( my string $input_string ) = @ARG; ###print {*STDERR} $input_string; }

1;    # end of package
