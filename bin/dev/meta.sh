#!/bin/bash

if false; then

# return all modules provided by RPerl distribution
curl -XPOST https://fastapi.metacpan.org/v1/release/_search -d '
{
  "query" : { "terms" : { "distribution" : [ "RPerl" ] } }, 

  "filter": {
    "and": [
      { "term": {"maturity": "released"} },
      { "term": {"status": "latest"} }
    ]
  },

  "fields": ["name", "distribution", "date", "version", "provides"]
}
'

fi




if true; then

# return module containing RPerl::Test package
curl -XPOST https://fastapi.metacpan.org/v1/module/_search -d '
{
    "query": {
        "nested" : {
            "path" : "module",
            "query" : {
                "bool" : {
                    "must" : [
                        { "match" : {"module.name" : "RPerl::Test" } }
                    ]
                }
            }
        }
    },

    "size": 5000,
    "_source": [ "name", "module.name", "module.version" ],
    "filter": {
        "and": [
            { "term": { "distribution": "RPerl" } },
            { "term": { "maturity": "released" } },
            { "term": { "status": "latest" } }
        ]
    }
}'

fi




if false; then

# return all RPerl modules
curl -XPOST https://fastapi.metacpan.org/v1/module/_search -d '
{
    "query" : {
        "constant_score" : {
            "filter" : {
                "exists" : { "field" : "module" }
            }
        }
    },

    "size": 5000,
    "_source": [ "name", "module.name", "module.version" ],
    "filter": {
        "and": [
            { "term": { "distribution": "RPerl" } },
            { "term": { "maturity": "released" } },
            { "term": { "status": "latest" } }
        ]
    }
}'

fi

