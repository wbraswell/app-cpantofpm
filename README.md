App::CPANtoFPM
==============

Create OS-Specific Packages From CPAN Distributions Using FPM

App::CPANtoFPM is Free & Open Source Software (FOSS), please see the LICENSE file for legal information:

<a href="https://gitlab.com/wbraswell/app-cpantofpm/blob/master/LICENSE">https://gitlab.com/wbraswell/app-cpantofpm/blob/master/LICENSE</a>

App::CPANtoFPM is part of the RPerl Family of software and documentation, please see the RPerl website for general information:

<a href="http://www.rperl.org">http://www.rperl.org</a>